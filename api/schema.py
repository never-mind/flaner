from graphene                import relay, ObjectType, Schema
from graphene_django.types   import DjangoObjectType
from graphene_django.filter  import DjangoFilterConnectionField

from core.models import *

class WorkspaceNode(DjangoObjectType):
    class Meta:
        model = Workspace
        filter_fields = ['name' ]
        interfaces    = (relay.Node, )

class Query(ObjectType):
    workspace      = relay.Node.Field(WorkspaceNode)
    all_workspaces = DjangoFilterConnectionField(WorkspaceNode)

schema = Schema(query=Query)