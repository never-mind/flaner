from wagtail.api.v2.views           import PagesAPIViewSet, BaseAPIViewSet
from wagtail.api.v2.router          import WagtailAPIRouter
from wagtail.images.api.v2.views    import ImagesAPIViewSet
from wagtail.documents.api.v2.views import DocumentsAPIViewSet

from .mixins import (
                        BaseAPIViewSetAuthMixin,
                        AuthImagesAPIViewSet,
                    )

from django.urls import path

class UUIDAPIViewSet(BaseAPIViewSet):
    """
    Extend wagtails BaseAPIViewSet to be able to handle UUID as primary key
    """
    @classmethod
    def get_urlpatterns(cls):
        return [
            path("", cls.as_view({"get": "listing_view"}), name="listing"),
            path("<uuid:pk>/", cls.as_view({"get": "detail_view"}), name="detail"),
            path("find/", cls.as_view({"get": "find_view"}), name="find"),
        ]


# Create the router. "wagtailapi" is the URL namespace
api_router = WagtailAPIRouter('wagtailapi')

# Add the three endpoints using the "register_endpoint" method.
# The first parameter is the name of the endpoint (such as pages, images). This
# is used in the URL of the endpoint
# The second parameter is the endpoint class that handles the requests
# api_router.register_endpoint('pages',     AuthPagesAPIViewSet)
api_router.register_endpoint('images',    AuthImagesAPIViewSet)
# api_router.register_endpoint('documents', AuthDocumentsAPIViewSet)


# Register custom snippet including authentication
from core.models import *

class WorkspaceAPIViewSet(BaseAPIViewSetAuthMixin, UUIDAPIViewSet):
    model = Workspace

class AttributeAPIViewSet(BaseAPIViewSetAuthMixin, UUIDAPIViewSet):
    model = Attribute

class EntityAPIViewSet(BaseAPIViewSetAuthMixin, UUIDAPIViewSet):
    model = Entity

class FileAPIViewSet(BaseAPIViewSetAuthMixin, UUIDAPIViewSet):
    model = File

api_router.register_endpoint('workspaces',  WorkspaceAPIViewSet)
api_router.register_endpoint('attributes',  AttributeAPIViewSet)
api_router.register_endpoint('entities',    EntityAPIViewSet)
# api_router.register_endpoint('files',       FileAPIViewSet)


from django.urls    import reverse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework.permissions     import IsAuthenticated, IsAdminUser

def absolute_reverse(name, request=False, **kwargs):
    return request.build_absolute_uri(reverse(name, kwargs=kwargs))

class EntitySerializer(serializers.ModelSerializer):

    class EntityAttributeField(serializers.RelatedField):
        def to_representation(self, value):
            return dict(
                id=value.attribute.id,
                key=value.attribute.key.name,
                value=value.attribute.value,
                files=[ 
                    dict(
                        id=fa.file.id,
                        title=fa.file.title,
                        url=absolute_reverse('file_serve', request=self.context.get('request'), file_id=fa.file.id)
                    ) for fa in value.attribute.fa_files.all()
                ]
            )

    classification = serializers.SerializerMethodField()
    attributes     = EntityAttributeField(source='ea_attributes', many=True, read_only=True)
    
    def get_classification(self, obj):
        return obj.classification.name
        
    class Meta:
        model = Entity
        fields = ['id', 'classification', 'reference', 'attributes']


class AttributeSerializer(serializers.ModelSerializer):

    class FileField(serializers.RelatedField):
        def to_representation(self, value):
            return dict(
                id=value.file.id,
                title=value.file.title,
                url=absolute_reverse('file_serve', request=self.context.get('request'), file_id=value.file.id))

    class EntityField(serializers.RelatedField):
        def to_representation(self, value):
            return dict(
                id=value.entity.id,
                classification=value.entity.classification.name,
                reference=value.entity.reference)

    key      = serializers.StringRelatedField(many=False)
    files    = FileField(source='fa_files',      many=True, read_only=True)
    entities = EntityField(source='ea_entities', many=True, read_only=True)

    class Meta:
        model = Attribute
        fields = ['id', 'key', 'value', 'files', 'entities']


class FileSerializer(serializers.ModelSerializer):

    class FileAttributeField(serializers.RelatedField):
        def to_representation(self, value):
            return dict(
                id=value.attribute.id,
                key=value.attribute.key.name,
                value=value.attribute.value)

    class OriginField(serializers.RelatedField):
        def to_representation(self, value):
            return value.url

    origin     = OriginField(source="origin_url", many=False, read_only=True)
    attributes = FileAttributeField(source="fa_attributes", many=True, read_only=True)
    workspace  = serializers.StringRelatedField(many=False)
    url        = serializers.SerializerMethodField()
    thumb_url  = serializers.SerializerMethodField()

    def get_url(self, obj):
        return absolute_reverse('file_serve', request=self.context.get('request'), file_id=obj.id)

    def get_thumb_url(self, obj):
        return absolute_reverse('file_thumb_serve', request=self.context.get('request'), file_id=obj.id)

    class Meta:
        model = File
        fields = [
            'id', 'title', 'url', 
            'workspace', 'origin',
            'file_size', 'file_hash', 'mimetype',
            'thumb_url', 'created_at', 'uploaded_by_user',
            'attributes',
        ]

class WorkspaceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Workspace
        fields = [
            'id', 'name', 'indented_name'
        ]



class AuthenticatedMixin:
    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action == 'list':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


class FileViewSet(AuthenticatedMixin, viewsets.ModelViewSet):
    serializer_class = FileSerializer
    # permission_classes = [IsAccountAdminOrReadOnly]
    # filter_backends  = [DjangoFilterBackend]
    queryset = File.objects.all()
    # def get_queryset(self):
    #     return self.request.user.accounts.all()
    # filter_backends = (CustomFilter, )

    def filter_queryset(self, queryset):
        # super needs to be called to filter backends to be applied
        queryset = super().filter_queryset(queryset)
        # some extra filtering
        return queryset


class AttributeViewSet(AuthenticatedMixin, viewsets.ModelViewSet):
    serializer_class = AttributeSerializer
    queryset         = Attribute.objects.all()

class EntityViewSet(AuthenticatedMixin, viewsets.ModelViewSet):
    serializer_class = EntitySerializer
    queryset         = Entity.objects.all()

class WorkspaceViewSet(AuthenticatedMixin, viewsets.ModelViewSet):
    serializer_class = WorkspaceSerializer
    queryset         = Workspace.objects.all()



from rest_framework.routers import DefaultRouter
rest_router = DefaultRouter()
rest_router.register('workspaces', WorkspaceViewSet)
rest_router.register('files',      FileViewSet)
rest_router.register('attributes', AttributeViewSet)
rest_router.register('entities',   EntityViewSet)