from wagtail.api.v2.views           import PagesAPIViewSet, BaseAPIViewSet
from wagtail.api.v2.router          import WagtailAPIRouter
from wagtail.images.api.v2.views    import ImagesAPIViewSet
from wagtail.documents.api.v2.views import DocumentsAPIViewSet

from rest_framework.permissions     import IsAuthenticated, IsAdminUser

class BaseAPIViewSetAuthMixin:
    """
    Mixin class to add permitions to wagtail rest api viewsets
    """
    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action == 'list':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

class AuthPagesAPIViewSet(BaseAPIViewSetAuthMixin, PagesAPIViewSet):
    pass

class AuthDocumentsAPIViewSet(BaseAPIViewSetAuthMixin, DocumentsAPIViewSet):
    pass

class AuthImagesAPIViewSet(BaseAPIViewSetAuthMixin, ImagesAPIViewSet):
    pass