from django.views.decorators.csrf    import csrf_exempt
from django.contrib.auth.mixins      import LoginRequiredMixin
# from graphene_django.views           import GraphQLView
from graphene_file_upload.django     import FileUploadGraphQLView

class PrivateGraphQLView(
                LoginRequiredMixin,

                # We use graphene-file-upload implemetation instead of 
                # the default GraphQLView to support file uploads 
                # https://github.com/lmcgartland/graphene-file-upload 
                FileUploadGraphQLView,
                
            ):
    pass

graphql_view = csrf_exempt(PrivateGraphQLView.as_view(graphiql=True))
