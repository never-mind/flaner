Requirements 
- docker

Run 

0. Clone repo and checkout to stable tag

```bash
git clone https://gitlab.com/never-mind/flaner
cd flaner
git checkout stable
```

1. Build container 

`docker build -t flaner .`

2. Initialize container 

```bash
docker run -it -v /tmp/data:/app/data  -e DB_PATH=/app/data/flaner.db -it flaner bash -c '
    ./manage.py migrate;
    ./manage.py shell <<EOF

from django.contrib.auth.models import User
user=User.objects.create_user("admin", password="admin")
user.is_superuser=True
user.is_staff=True
user.save()

from wagtail.users.models import UserProfile  
up = UserProfile()
up.user_id = user.id
up.theme   = "light"
up.save()

exit()
EOF

'
```

3. Run server

```bash
docker run -it \
    --name=flaner \
    -v /tmp/data:/app/data \
    -p 8000:8000 \
    -e DB_PATH=/app/data/flaner.db \
        flaner ./manage.py runserver 0.0.0.0:8000`
```

4. Point your browser to http://127.0.0.1:8000/ and login using 'admin' as username and password.

5. To test automatic Webpage download feature need to install chromium inside the container.

```bash
docker exec -it flaner bash -c 'apt update;apt install -y chromium;'
```