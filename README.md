# Welcome to Flaner project

> *In times of terror, when everyone is something of a conspirator, everybody will be in a situation where he has to play detective.*
>
> **Walter Benjamin**, The Flâneur (1938)


Current repository includes a django application, that serves as a prototype of Flaner project.

The prototype includes the core features of the proposed project, under a functional user interface.

For anyone that wish to try out the code checkout the [GETTING-STARTED.md](GETTING-STARTED.md) instructions.

<!-- Design docs, issues and other documents can be found under [docs](docs.mk/index.md) -->

Following videos demonstrate the prototype.


1. Example on creating workspaces

![](docs.mk/screenshots/videos/workspaces.webm)
<!-- <video controls width="580" controls preload="none">
  <source src="https://gitlab.com/never-mind/flaner/-/raw/main/docs.mk/screenshots/videos/workspaces.webm" type="video/webm" />
</video> -->

2. Create pdf file and add entity and time based annotations. 

![](docs.mk/screenshots/videos/create-file.webm)
<!-- <video controls width="580" controls preload="none">
  <source src="https://gitlab.com/never-mind/flaner/-/raw/main/docs.mk/screenshots/videos/create-file.webm" type="video/webm" />
</video> -->

3. Edit pdf file and add location annotations

![](docs.mk/screenshots/videos/annotate-locations.webm)
<!-- <video controls width="580" controls preload="none">
  <source src="https://gitlab.com/never-mind/flaner/-/raw/main/docs.mk/screenshots/videos/annotate-locations.webm" type="video/webm" />
</video> -->

4. Create Map presentation from created locations

![](docs.mk/screenshots/videos/map-presentation.webm)
<!-- <video controls width="580" controls preload="none">
  <source src="https://gitlab.com/never-mind/flaner/-/raw/main/docs.mk/screenshots/videos/map-presentation.webm" type="video/webm" />
</video> -->

5. Create custom annotation type and annotate video file

![](docs.mk/screenshots/videos/video-annotation.webm)
<!-- <video controls width="580" controls preload="none">
  <source src="https://gitlab.com/never-mind/flaner/-/raw/main/docs.mk/screenshots/videos/video-annotation.webm" type="video/webm" />
</video> -->