# Test Django Interface :test-dj-int
1. Try out xadmin to customize django admin and check whether this is enough to build a admin only interface that 
incorporates all features.
https://github.com/sshwsfc/xadmin/
https://django-jazzmin.readthedocs.io/

2. Interesting Django extentions to build a simple and light admin only interface. 
+ https://github.com/raagin/django-streamfield
+ https://github.com/fabiocaccamo/django-treenode
+ https://github.com/django-polymorphic/django-polymorphic
+ https://github.com/Koed00/django-q
+ https://github.com/fabiocaccamo/django-admin-interface
+ https://books.agiliq.com/projects/django-admin-cookbook/en/latest/many_fks.html
popup
+ https://docs.djangoproject.com/en/4.0/ref/contrib/admin/#django.contrib.admin.ModelAdmin.autocomplete_fields
+ https://django-admin-autocomplete-all.readthedocs.io/en/latest/usage.html
+ https://django-ajax-selects.readthedocs.io/en/latest/Forms.html
+ https://github.com/djk2/django-popup-view-field
+ https://github.com/openwisp/django-loci/blob/2f36fca6a73bd0fef1f78da3cae7502546a1cb15/django_loci/base/admin.py#L33
+ https://github.com/erdem/django-map-widgets/blob/v0.2.0/mapwidgets/widgets.py
+ https://gist.github.com/beratdogan/bd57ef57e53b12b48d9e
+ https://agusmakmun.github.io/python/django/2020/05/12/how-setup-all-raw-id-fields-and-search-fields-for-django.html
+ https://gist.github.com/mrts/290224/0be470098e20734b97235f325c9afcc1624e16d2
+ Monaco editor
    + https://github.com/noteby/django-monaco-editor/blob/master/django_monaco_editor/widgets.py
    + https://github.com/giorgi94/django-editor-widgets

# Intergrate Location :loc-int

Openwisp has an amazing django module for location based 
interface named [django-loci](https://github.com/openwisp/django-loci).

Intergrate it with GeoDjango and spatialite

Another interesting library is [django-uuid-address](https://github.com/OmenApps/django-uuid-address), obviously not as supported and featurefull as openwisp's loci


# Add trix rich-text-editor

+ [django-prose](https://github.com/withlogicco/django-prose/)
