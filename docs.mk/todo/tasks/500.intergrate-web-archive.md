## Description

Intergrate WebArchive into File model. Webrecorder group has opensourced several amazing tools for the people to be able to record and replay web page content. 

The ticket suggest the following intergration
- Users record their content on their own and produce .wacz files
- Users upload .wacz as a File
- File can be previewed and annotated


WebRecorder (user recording)

User may install ArchiveWeb.page as an application [releases](https://github.com/webrecorder/archiveweb.page/releases/tag/v0.11.0) or as a Chrome extention [link](https://chrome.google.com/webstore/detail/webrecorder-archivewebpag/fpeoodllldobpkbkabpblcfaogecpndd)


Serve Web Archive Files

File model should Intergrate [web-replay-gen](https://github.com/webrecorder/web-replay-gen) with Wagtail Panels to allow users preview and annotate their recorded content. web-replay-gen is suggested over replayweb.page [embedded method](https://replayweb.page/docs/embedding) as it may contain several recorded pages in contrast to embedded mode where only a single page can be previewed.
It would be nice to add django management cli for admins to automatically crawl content. 
 

Browsertrix Crawler (automated crawling)
 
Additionally to WebRecorder we might use [browsertrix-crawler](https://github.com/webrecorder/browsertrix-crawler) to automate the crawling of a url or even mirror a full website. We can add it in automation.



Automatic user upload 

Additionally we could implement browsertrix-cloud's api [link](https://github.com/webrecorder/browsertrix-cloud/blob/main/backend/btrixcloud/uploads.py#L238). Then, users would configure their app's / extention's settings and point Browsertrix Cloud to our api, instead of downloading and uploading their archives manually.
