# Description

Investigate [projectcaluma](https://github.com/projectcaluma) and it's subprojects especially [alexandria](https://github.com/projectcaluma/alexandria).
It has a nice simple model for document management using s3 as a backend.
