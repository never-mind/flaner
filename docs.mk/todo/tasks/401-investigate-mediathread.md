## Description

> Mediathread is a Django site for multimedia annotations facilitating collaboration on video and image analysis. Developed at the Columbia Center for New Media Teaching and Learning (CCNMTL)

[source-code](https://github.com/ccnmtl/mediathread)

Mediathread is a very interesting application - bui8ld for university cources - that provide annotations for a range of files (videos, images, pdfs). We could get inspiration or even pieces of code and intergrate it on our project.
 
