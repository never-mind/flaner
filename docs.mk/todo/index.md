# TODO

> List of Active, Prioritized, Rejected tasks to organize development  


## Prioritized

- [ ] remove workspace permittions on prototype
- [ ] Automatically scale the file preview to fit inside canvas width and height. 
- [ ] Make animated annotation that renders on annotation click, more visually intence.
- [ ] Add additional button which will optionally attach mouse selection to annotation ( not automatically as it happens now ).
- [ ] Simplify menu

## Backlog

> Here we keep a list of tasks that are not yet prioritized.

- [ ] [028 Consider adding other presentations](tasks/028.Consider-adding-other-presentations.md)
- [ ] [032 investigate mozilla pdfjs annotations options](tasks/032.investigate-mozilla-pdfjs-annotations-options.md)
- [ ] [026 implement subfiles](tasks/026.implement-subfiles.md)
- [ ] Sync origin and file workspace field on change
- [ ] [025 implement weight model](tasks/025.implement-weight-model.md)
- [ ] [400 investigate documentcloud intergration](tasks/400.investigate-documentcloud-intergration.md)
- [ ] [401 investigate mediathread](tasks/401-investigate-mediathread.md)
- [ ] Define releases management
- [ ] Add docker/docker-compose/podman instructions for different environments
- [ ] Add sytemd, n-spawn instructions for different environments
- [ ] [402 add oembeed](tasks/402.add-oembeed.md)
- [ ] [403 use google knowledge graph](tasks/403.use-google-knowledge-graph.md)
- [ ] [404 investigate projectcaluma](tasks/404-investigate-projectcaluma.md)
- [ ] on AnnotationPanel file preview place files in the center of canvas and not on top-left corner (for show/zoom).

## Releases

> Not really defined yet.

## DONE

> List of completed (x), or rejected (-) tasks

- [x] 2023-??-?? Setup Enhancements module
- [x] 2023-??-?? Setup File module
- [x] 2023-??-?? Setup Entity module
- [x] 2023-05-29 Setup MkDocs
- [x] 2023-05-29 [005 setup lightweight scheduler](tasks/005.setup-lightweight-scheduler.md)
- [x] 2023-05-31 Define different environments, ( minimal, embedded, distributed, k8s, p2p, ... )
- [x] 2023-05-31 [006 change lightweight scheduler](tasks/006.change-lightweight-scheduler.md)
- [x] 2023-05-31 [010 URL Download Task](tasks/010.url-download-task.md)
- [x] 2023-05-31 Autogenerate thumbnail and guess mimetype on File creation
- [x] 2023-06-01 Add basic views for Time, Place Annotations
- [x] 2023-06-04 [020 Add annotation view](tasks/020.add-annotation-view.md)
- [x] 2023-06-09 Add graph presentation
- [x] 2023-06-21 [021 Improve Edit and Read view](tasks/021.improve-edit-read-view.md)
- [x] 2023-06-22 Move /wagtail/ admin endpoint directly on /
- [x] 2023-06-22 Replace browser with chromium --headless --print-to-pdf ....
- [-] [008 setup lightweight workflow ](tasks/008.setup-lightweight-workflow.md)
- [-] [012 make url download more flexible](tasks/012-make-url-download-more-flexible.md)
- [x] 2023-06-23 [030 migrate attributes](tasks/030.migrate-attributes.md)
- [x] 2023-06-23 [022 Implement some basic fingerprint models](tasks/022.implement-fingerprint-models.md) 
- [x] 2023-07-15 [011 dynamic preview file view](tasks/011.dynamic-preview-file-view.md)
- [x] 2023-07-16 [031 study web annotation and archiving standards](tasks/031.study-web-annotations-and-archiving-standards.md)
- [-] 2023-08-18 Add read only views for events, locations, attributes
- [-] 2023-08-18 Improve Address model, using https://github.com/OmenApps/django-uuid-address, wagtailgeowidget ?
- [-] 2023-08-18 Add recursive filtering on parent workspaces for files. No sure yet, as single workspace filtering is helpfull on some use-cases.
- [-] 2023-09-07 [027 Add presentations](tasks/027.Add-presentations.md)
- [-] 2023-09-15 Improve AnnotationPanel to discover annotation targets
- [-] 2023-09-15 Add raw text file preview
