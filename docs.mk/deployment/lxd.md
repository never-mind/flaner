# LXD

While kubernetes contains loads of features that helps with scheduling and high-availability the 
it overcomplicates things for a small deployment and to reach a secure environment is quite complex.

Therefore LXD, which suppports both containers and VMs is a much simpler sollution, tightly intergrated 
to the system itself and therefore easier to manage and understand the implications. 


[Clustering](https://linuxcontainers.org/lxd/docs/master/clustering/)

Monitoring using [prometheus](https://linuxcontainers.org/lxd/docs/master/metrics/#)

[Backups](https://linuxcontainers.org/lxd/docs/master/backup/)

Setup Ceph using [microceph](https://canonical-microceph.readthedocs-hosted.com/en/latest/)


## LXD Dashboard

A nice dashboard for LXD is provided by https://lxdware.com/

The dashboard would be fit for a controller node that is NOT part of the LXD cluster and 
connects to the remote nodes [link](https://lxdware.com/adding-remote-hosts-in-the-lxd-dashboard/)


## SECURITY

TPM support for [certificates](https://linuxcontainers.org/lxd/docs/master/reference/devices_tpm)

LUKS Encryption using [clevis](https://manpages.ubuntu.com/manpages/jammy/man1/clevis.1.html) and [tang](https://manpages.ubuntu.com/manpages/bionic/man8/tang.8.html). Tang can be placed either on a FreeBSD/PFSense Router, or a controller. 


## Other

- PROXMOX https://pve.proxmox.com/wiki/Deploy_Hyper-Converged_Ceph_Cluster
- opennebula https://opennebula.io/discover/#opennebula_overview

