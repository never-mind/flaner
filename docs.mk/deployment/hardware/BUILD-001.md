
# SERVER BUILD


190 motherboard
158 memory
225 processor
67  psu
43  nvme

143 case

- [motherboard][motherboard]
    
    * Intel 1700  (Alder Lake, Raptor Lake) - Intel Z790, -> Core i3, Core i5, Core i7, Core i9, Celeron Dual Core, Pentium Dual Core
        
        - [processor][processor] i5-13400F 1.8GHz 10 Πυρήνων για Socket 1700

    * Τύπος Μνήμης: DDR5, Dual Channel, Ταχύτητες Μνήμης: 4400 MHz, 4800 MHz, 5200OC MHz, 5600OC MHz, 6400OC MHz, 6600OC MHz
        
        - [memory][memory] 2 * 16Gb DDR5, Ταχύτητa: 6400 / CAS: 32  

    * Τύπος M.2: 4 Θύρες PCIe 4.0
        
        - [nvme][nvme]

    * 8 Port SATA III 6Gb/s

- [psu][psu] Be Quiet 600W ATX

- [case][case]


[nvme]: https://www.skroutz.gr/s/37931416/Kingston-NV2-SSD-1TB-M-2-NVMe-PCI-Express-4-0-SNV2S-1000G.html "Kingston NV2 SSD 1000GB M.2 NVMe PCI Express 4.0"
[motherboard]: https://www.skroutz.gr/s/39049285/Asrock-Z790-Pro-RS-Motherboard-ATX-%CE%BC%CE%B5-Intel-1700-Socket.html#specs "ASRock Z790 Pro RS Motherboard ATX με Intel 1700 Socket"
[memory]: https://www.skroutz.gr/s/40123153/Corsair-Vengeance-32GB-DDR5-RAM-%CE%BC%CE%B5-2-Modules-2x16GB-%CE%BA%CE%B1%CE%B9-%CE%A4%CE%B1%CF%87%CF%8D%CF%84%CE%B7%CF%84%CE%B1-6400-%CE%B3%CE%B9%CE%B1-Desktop-CMK32GX5M2B6400C32.html#specs "Corsair Vengeance 32GB DDR5 RAM με 2 Modules (2x16GB) και Ταχύτητα 6400 για Desktop"
[processor]: https://www.skroutz.gr/s/40221960/Intel-Core-i5-13400F-1-8GHz-%CE%95%CF%80%CE%B5%CE%BE%CE%B5%CF%81%CE%B3%CE%B1%CF%83%CF%84%CE%AE%CF%82-10-%CE%A0%CF%85%CF%81%CE%AE%CE%BD%CF%89%CE%BD-%CE%B3%CE%B9%CE%B1-Socket-1700-%CF%83%CE%B5-%CE%9A%CE%BF%CF%85%CF%84%CE%AF.html "Intel Core i5-13400F 1.8GHz Επεξεργαστής 10 Πυρήνων για Socket 1700 σε Κουτί"
[psu]: https://www.skroutz.gr/s/19535758/Be-Quiet-System-Power-9-CM-600W-%CE%A4%CF%81%CE%BF%CF%86%CE%BF%CE%B4%CE%BF%CF%84%CE%B9%CE%BA%CF%8C-%CE%A5%CF%80%CE%BF%CE%BB%CE%BF%CE%B3%CE%B9%CF%83%CF%84%CE%AE-Semi-Modular-80-Plus-Bronze.html#description "Be Quiet System Power 9 CM 600W Τροφοδοτικό Υπολογιστή Semi Modular 80 Plus Bronze"
[case]: https://www.skroutz.gr/s/5630565/Silverstone-GD10-Home-Theater-%CE%9A%CE%BF%CF%85%CF%84%CE%AF-%CE%A5%CF%80%CE%BF%CE%BB%CE%BF%CE%B3%CE%B9%CF%83%CF%84%CE%AE-%CE%9C%CE%B1%CF%8D%CF%81%CE%BF.html "Silverstone GD10 Home Theater Κουτί Υπολογιστή Μαύρο"





# NOTES 

This is the first build. If components work as expected and build runs, will order 2 more.

Once they are all built, need one more step, to wire them.
Will use 10Gps networking for that with following 
1. TP-LINK TL-SX1008 v1 Unmanaged L2 Switch με 8 Θύρες Gigabit (10Gbps) Ethernet
    https://www.skroutz.gr/s/27085759/TP-LINK-TL-SX1008-v1-Unmanaged-L2-Switch-%CE%BC%CE%B5-8-%CE%98%CF%8D%CF%81%CE%B5%CF%82-Gigabit-10Gbps-Ethernet.html

2. TP-LINK v1 Ενσύρματη Κάρτα Δικτύου Gigabit (10Gbps) Ethernet PCI-e
    https://www.skroutz.gr/s/25754975/TP-LINK-TX401-v1-%CE%95%CE%BD%CF%83%CF%8D%CF%81%CE%BC%CE%B1%CF%84%CE%B7-%CE%9A%CE%AC%CF%81%CF%84%CE%B1-%CE%94%CE%B9%CE%BA%CF%84%CF%8D%CE%BF%CF%85-Gigabit-10Gbps-Ethernet-PCI-e.html

