Graphics Card
- Gigabyte VGA GeForce RTX 3060 Eagle OC V2 12 GB
- https://www.plaisio.gr/anavathmisi-diktia/anavathmisi-pc/kartes-grafikon/gigabyte-vga-geforce-rtx-3060-eagle-oc-v2-12-gb_3755118
- 389,00

CPU
- https://www.plaisio.gr/anavathmisi-diktia/anavathmisi-pc/epeksergastes/intel-cpu-core-i7-13700k-1700-3-4-ghz-30-mb-_4052943
- Intel CPU Core i7 13700K (1700/3.4 GHz/30 MB)
- 469,00

PSU
- https://www.plaisio.gr/anavathmisi-diktia/anavathmisi-pc/trofodotika/corsair-psu-rmx-series-1000-w-80-gold-version-2021_3764397
- Corsair PSU RMX Series 1000 W 80+ Gold Version 2021
- 229,90 €

RAM
- https://www.plaisio.gr/anavathmisi-diktia/anavathmisi-pc/mnimes-ram/g-skill-desktop-ram-ripjaws-s5-64gb-kit-6000-mt-s-ddr5_4233859
- G.Skill Desktop RAM Ripjaws S5 64GB Kit 6000 MT/s DDR5
- 299,90 €

Cooler
- Corsair CPU Cooler Hydro H150iPro RGB XT
- https://www.plaisio.gr/anavathmisi-diktia/anavathmisi-pc/anemistirakia-psiktres/corsair-cpu-cooler-hydro-h150ipro-rgb-xt_3590291
- 149,90 €



Motherboard
- https://www.plaisio.gr/anavathmisi-diktia/anavathmisi-pc/mitrikes-kartes/gigabyte-motherboard-z790-gaming-x-z790-1700-ddr5-_4147707
- Gigabyte Motherboard Z790 GAMING X (Z790/1700/DDR5)
- Official: https://www.gigabyte.com/Motherboard/Z790-GAMING-X-AX-rev-1x/sp#sp
- 264,90 €

Case
- Corsair Case 4000D Airflow
- https://www.plaisio.gr/anavathmisi-diktia/anavathmisi-pc/koutia-desktop/corsair-case-4000d-airflow_3545350
- 124,90



HARD-DISK
  NVME
  - https://www.plaisio.gr/anavathmisi-diktia/apothikeusi/diskoi-ssd-hdd/corsair-ssd-mp600-pro-nh-2tb-pcie-4-0-m-2-2280-nvme_4101707
  - Corsair SSD MP600 PRO NH 2TB PCIe 4.0 M.2 2280 NVMe
  - 209,90 €
  - https://cosmodata.gr/product/385654/hardware-anavathmisi/diski/ssd-solid-state-diski/diskos-ssd-kingston-fury-renegade-sfyrdk-2000g-me-heatsink-2tb-m-2-2280-pcie-4-0-x4-nvme-ps5-ready#specs_title
  - Δίσκος SSD Kingston FURY Renegade (SFYRDK/2000G) με Heatsink - 2TB M.2 2280 - PCIe 4.0 x4 NVMe - PS5™ ready 
  - 194 .50€
  SSD
  - https://www.plaisio.gr/anavathmisi-diktia/apothikeusi/diskoi-ssd-hdd/samsung-ssd-870-evo-4tb_3748677
  - https://cosmodata.gr/product/368378/hardware-anavathmisi/diski/ssd-solid-state-diski/diskos-ssd-samsung-870-evo-mz-77e4t0b-eu-2-5-sata-3-4tb
  - Δίσκος SSD Samsung 870 Evo (MZ-77E4T0B/EU) - 2.5" SATA 3 - 4TB
  - 259,90 €

TPM
- https://www.skroutz.gr/s/28132497/Gigabyte-Accessory-GC-TPM2-0-TPM.html
- Gigabyte Accessory GC-TPM2.0 TPM

PCIe
- 10 SATA
  - https://www.skroutz.gr/s/25256586/DeLock-%CE%9A%CE%AC%CF%81%CF%84%CE%B1-PCIe-%CF%83%CE%B5-10-%CE%B5%CF%83%CF%89%CF%84%CE%B5%CF%81%CE%B9%CE%BA%CE%AD%CF%82-%CE%B8%CF%8D%CF%81%CE%B5%CF%82-SATA-III-89384.html
