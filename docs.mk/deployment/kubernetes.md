
- K8s
    - k3s
    - k0s
    - microk8s
- Storage
    - [openebs](https://openebs.io/docs/user-guides/quickstart)
    - Ceph using [rook](https://rook.io/)
- Secrets
    - [sealed-secrets]https://github.com/bitnami-labs/sealed-secrets#overview
