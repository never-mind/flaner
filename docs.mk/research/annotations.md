# Annotations

Current document research solutions on how to use annotations collaboratively.


## Web-Annotations

- [annotorious](https://recogito.github.io/annotorious/)
    - It is frontend only
    - Can be used for annotations as directus extention
    - Has Amazing tool for images. 

- [dokieli](https://github.com/linkeddata/dokieli)
    - web-extention
    - decentralized they say
    - storage ?

- [annotatorjs](http://annotatorjs.org/)
    - Base for hypothesis

- Interesting
    - [solidproject](https://solidproject.org/)
    - [brat](http://brat.nlplab.org/index.html)



## General Annotations


- [universaldatatool](https://docs.universaldatatool.com)
- [labelstudio](https://labelstud.io/)



### Other

Awesome lists 

- [Awesome web-annotation](https://github.com/jankaszel/web-annotation-ecosystem)


Web Browser based Extentions

- [webscrapbook](https://github.com/danny0838/webscrapbook) is a very limited annotation tool with a backend server to store annotations
	- [firefox](https://addons.mozilla.org/en-US/firefox/addon/webscrapbook/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search) Extention
	- [server](https://pypi.org/project/webscrapbook/)

- hypothesis.is
- [firefox](https://addons.mozilla.org/en-US/firefox/addon/worldbrain/)
- [Tldraw](https://github.com/Tldraw/Tldraw) can draw on web pages but doesn't export.

