# Syncing files

An interesting approach to handle storage and exchange file in an user friendly and secure manner is to use [syncthing](https://docs.syncthing.net/). 

It could propably be set to use private certificate authority and run behing 
ceph to handle unlimited storage. 

It has an [api](https://python-syncthing.readthedocs.io/en/latest/#) that exposes events and other information.

