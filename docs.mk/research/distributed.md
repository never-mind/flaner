# Distributed

Reseach ways on how to setup distributed architecture


One of the reasons for distributed setup is to be able to communicate with remote devices 
(like raspberries, browser extentions, vps and others) and send them jobs. The jobs should
be able to interact during execution. 


## Remote browser

The most important feature that needs distributed architecture is the ability to control 
multiple remote browser that each browser has an attached identity.

An inspiring implementation using django-channels and pyppeteer is [rippy](https://github.com/JohnDoee/rippy).
It contains an advanced implementation of pausing for captchas [here](https://github.com/JohnDoee/rippy/blob/master/rippy/extractors/avgle.py#L48).


## Wamp Protocol

Wamp is a nice idiomatic pub/sub and rpc on top of websockets,
that makes it easy and lightweight solution to build real-time destributed applications.

While crossbar defined the protocol and used to develop the autobahn router, they
moved to a closed crossbar router (Version 2) implementation which is not a way to go. 

1. [wampyre](https://github.com/JohnDoee/wampyre) is nice lightweight Wamp router intergrated to Django channels. 

