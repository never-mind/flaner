# Research 

Directory contains research on ideas, drafts, tools that is done
before deciding on specific implementations.

## Contents 

- [distributed](distributed.md)
- [workflow](workflow.md)
