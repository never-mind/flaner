Golang p2p scraper
==================

This document proposes the replacement of SQL to a distributed database.

The most promising decentralized Community/Protocol is [DAT](https://github.com/datproject/organization) with Hypermedia Division, where IPFS is build upon.
The most promising Database is [ThreadDB][1] which runs on top of IPFS. 

Features to consider
--------------------

1. The project would leave the posibility to create different Distributed databases.
2. Operators should be able to join different databases (by federation ? by replication ?), where the will have read/write access based on aggrement. 
3. Operators should not handle scrapers/scanners/implants or any process that collects data.
4. Warriors should handle scrapers/scanners/implants and push data back to operators
5. Warriors could be Investigators.
6. Investigators should be able to search collected data from databases on agreement with operator. 
7. All parties should be able to remain pseudonymous during their collaboration/agreements
8. Data should be signed (and posibly encrypted) and readable on aggrement (per collection).


Database
--------

If we are about to change the Database we need to redesign the data structure.
In the case of [ThreadDB][1] that is similar to MongoDB, that would handle items/relationships as Documents and Graphs/Cases as collections.
[ThreadDB][1] Features:
- Self host on top of IPFS
- Writer Validation, Reader Filtering (Who is responsible for writing those?)
- Replication without read access to content


Other Databases includes
- [OrbitDB](https://github.com/orbitdb/orbit-db)
	A good starting point would be https://github.com/orbitdb/example-orbitdb-todomvc-updated
- [AvionDB](https://github.com/dappkit/aviondb) runnning on top of OrbitDB.
    - write access control should go to trustees ? to everyone ? start with the owner only.
- [Scuttlebot](https://scuttlebot.io/)
- Search-able DB [Sonar](https://arso.xyz/sonar)-[code](https://github.com/arso-project/sonar)
- https://framagit.org/hubzilla/core


Storage
-------
- File Storage  https://gitlab.com/coboxcoop/server (hypermedia)
- File Browser  https://docs.beakerbrowser.com/


Crawling
--------

goquery and [colly](http://go-colly.org/docs/) are nice tools for crawling but the winner seems to be:

[Ferret](https://github.com/MontFerret/ferret) is very modular using custom scripting language, such as
https://github.com/MontFerret/ferret/blob/master/examples/google-search.fql

The Framework should use a distribution with gpg-signed plugins which users can list and execute and contain 
Ferret scripts. Ferret scripts should produce json objects.

Json objects should be visualized first, with the option to publish them to core DB.

[1]: https://github.com/textileio/go-threads/
