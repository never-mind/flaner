Current document describes the functionality of Flaner software from a user perspective.

Generally, Flaner software can be think as a shared data entry platform. It runs as a service, where users can access the User Interface from their web browser.

## Access the UI

The whole UI is protected behind a user authentication page, where only authhenticated users can make use of the platform. 

## Setup

Before anything else some setup needs to take place.

Administrators should follow [Setup](setup) instructions to create users and configure the instance. 

Once set, users may follow the instructions below


## Explore the UI

The UI is splitted into following main sections 

1. [Files and Workspaces](files-workspaces)
2. [Annotations](annotations)
3. [Presentations](presentations)
