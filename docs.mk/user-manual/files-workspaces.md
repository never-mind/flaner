
- [Workspaces](#workspaces)
- [Files](#files)
- [Origins](#origins)


# Workspaces

Workspaces can be think as folders. They have a tree-like hierarchy where each workspace can contain more workspaces. Similar to folders, a workspace may contain multiple files but each file is linked to one and only one workspace.

Their purpose is to allow users organize their files, in a way that make sense to them. 

Following is an example workspace setup using Watergate Scandal as a paradigm

![](/screenshots/workspace-list-example.png)


To create a new workspace, click on **workspaces** on left-side menu, then click on **Add new workspace** button. Then fill the Name field, choose the parent workspace, and click save.
The new workspace will be listed below the chosen parent (see the example above).

If we want to rearrange an existing workspace and move it under a different parent, all sub-workspaces will be moved together. Following the example above, by moving *Watergate Scandal* workspace to *1972* workspace, all sub-workspaces are moved as well

![](/screenshots/workspace-move-example.png)



# Files

By clicking on "Files" in the left menu bar, users can manage their files. 

List files

## Upload a  new file

First click **Add file** button

![](/screenshots/add-new-file-button.png)

Then fill the form to upload a new file. 

![](/screenshots/add-new-file.png)

## Edit File

...


# Origins

Origin describes where a file originates from.

Origin is an URL (Unique Resource Locators) that are useful to track from what webpage a file was downloaded from.

There are 2 ways to set the origin.

1. They can be set directly when we upload or edit files. 
2. Can be created from 'origin' menu

The difference is on 2nd case, when we create a new origin, we have the option to automatically download the file included in the url.

See following example, on how to create an origin that downloads a pdf file.

![](/screenshots/origin-download.png)

Once we save the origin a new file will be created and can be found listed first under files,
or if we open the origin, we can see it listed under **Files** field.

![](/screenshots/origin-link.png)


It is suggested to always check the downloaded file is properly created, and might change the file title to something that makes sense.