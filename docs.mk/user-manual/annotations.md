
Annotations is the core part of Flaner. They can be think as metadata - semantics - that users can attach to uploaded files. Unlike files, annotations are not part of any workspace.

Annotations are modeled after traces concept, and are divided into 3 following types. If you care about the design you can read more [here](/design/traces).

1. [Attributes](#attributes) unique identifiers of an Entity
2. [Events](#events) a point in time. 
3. [Locations](#locations) a place on world map. 

All different annotation types can be created while we edit a file.

# Attributes

Attributes can be think as unique identifiers of an Entity, where entities can be think as subject of interest.

An Attribute, have a type that user choose and a value that user fill. The diffenent types are configured by user, based on their interest.

For example if the subject we are interested about is an *Organization*, its *Legal Name* would be an attribute we can use. *Vat Registration Number* could be another attribute we can use.

Similar, in case we are interested in a Car, its *Model Name* and *Plate Number* could be 2 attributes we can use.

Before we add any Attribute, we need to define the different types of *Attribute Definitions*. If no *Attribute Definitions* are setup, follow [setup guide](/user-manual/setup#setup-attribute-definitions) to do so.  


--- 

# Events

Events are describing time events.

While we research a subject might know the exact time of an event, or might know that the event took place between a time period bun not the exact time. 

When we create an event, apart from the title - that describe the event we need to set either the *exact time* field or both of the *start*, *end* fields.


--- 

# Location

TBD

--- 

# Entities

Entities are groups of annotations. They contain a list of attributes, locations and events.

While they are not directly attached on files, they help us to organize different annotations in a sensible manner. 

---