Before start using Flaner, there certain configuration needs to happen.

1. [Setup Attribute Definitions](#setup-attribute-definitions)
2. [Setup Entity Definitions](#setup-entity-definitions)
3. [User Management](#user-management)

--- 


# Setup Attribute Definitions

Attribute definitions (or attribute names) describe the different categories of attributes that users can create. To be able to create Attributes, we first need to create an Attribute Definition.

*You can read more about attributes under [annotations section](/user-manual/annotations#attributes)*

An Attribute can be considered as unique identifiers of an entity. For example if an entity is an organization, the 'legal name' that the org is registered as, is an unique identifier. Another example
could be an email address the organization uses. 

On this example we create an Attribute definition for Email Addresses. Click on `Annotations` on left-side menu, then `Attribute Definitions` and click on `Add attribute name` button.
To create a new Entity Definition, click on `Annotations` on left-side menu, then `Entity Definitions` and last on `Add entity classification` button. Then,

1. Choose **mail** as relevant Icon 
2. Fill "Email Address" into Name field
3. Click Save

---

# Setup Entity Definitions

Entity Definitions (or entity classifications), define the different categories an entity can have.

*You can read more about entities under [annotations section](/user-manual/annotations#entities)*

Some examples of entity definitions could be Person, Organization, Car. 

To create a "Person" Entity Definition, click on `Annotations` on left-side menu, then `Entity Definitions` and last on `Add entity classification` button. Then,

1. Fill "Person" into Name field 
2. Choose **person** as relevant Icon


---

# User Management

Administrators are responsible to setup Users and Groups.

Users can have different priviledges, based on Group they are part of.

To get up and working, create a group with permittions listed in the following picture

![](/screenshots/group-example-permittions.png)

*Important: Do not forget to enable access to wagtail admin, by clicking the checkbox under Other Permissions > 'Can access wagtail admin'*

Then create new users and assign them the group we just created.

Users after initial login should change their password.

---
