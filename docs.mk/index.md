# Welcome to Flaner project

> *In times of terror, when everyone is something of a conspirator, everybody will be in a situation where he has to play detective.*
>
> **Walter Benjamin**, The Flâneur (1938)

<!-- 
![](diagrams/Annotations.png) -->



## Introduction

The project is currently in its **early development stage**. 

The current phase can be considered as a prototype - a working proof of concept. The main aim of the prototype is to demo the core functionality of the project, while we can collect all different requirements before we begin the actual development phase.  

The current prototype is build using [Wagtail CMS](https://wagtail.org) framework (a django based web framework for python programming language).


## Table Of Contents

- [Design](design/)
- [User Manual](user-manual/)

<!-- 

The most important files that describe the core functionality are the following 

- [Document-Sharing-Platform](functionality/Document-Sharing-Platform.md)
- [Annotations](functionality/Annotations.md)

Additionally a vewry basic but helpful issue tracking is stored under [Todo](todo/index.md)

[Code Reference](reference/index.md) 

 -->
