from wagtail.admin.panels import InlinePanel, ObjectList, FieldPanel
from wagtail.admin.panels       import Panel, InlinePanel, FieldPanel, ObjectList, MultipleChooserPanel, TabbedInterface

from wagtail.snippets.views.snippets import SnippetViewSet

from ..models import Presentation
from ..panels import PresentationPanel

class PresentationViewSet(SnippetViewSet):
    add_to_admin_menu = True
    menu_order   = 5
    icon         = 'clipboard-list'
    model        = Presentation
    edit_handler = TabbedInterface([
                            ObjectList([
                                PresentationPanel(),
                            ], heading='Main'),
                            ObjectList([
                                FieldPanel('name'),
                                FieldPanel('slug'),
                                FieldPanel('presentation_type'),
                                InlinePanel('files', heading='Files'),
                                InlinePanel('entities', heading='Entities'),
                                InlinePanel('locations', heading='Locations'),
                                InlinePanel('events', heading='Events'),
                            ], heading='Details'),
                        ])
