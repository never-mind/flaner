from wagtail.snippets.views.snippets import SnippetViewSet
from wagtail.admin.panels            import FieldPanel, InlinePanel
from wagtail.admin.views.generic.chooser import ChooseView, ChosenView, ChosenMultipleView

from ..models import *



class AttributeNameViewSet(SnippetViewSet):
    icon          = 'cogs'
    model         = AttributeName
    list_display  = ("name", "admin_thumb_icon", "pattern",)    
    menu_label = 'Attribute Definitions'

class EntityClassificationViewSet(SnippetViewSet):
    icon          = 'cogs'
    model         = EntityClassification
    list_display  = ("name", "admin_thumb_icon",)
    menu_label = 'Entity Definitions'

class AttributeViewSet(SnippetViewSet):
    icon          = 'code'
    model         = Attribute
    list_display  = ("name", "value",)
    list_filter   = ('key',)

    edit_handler = ObjectList([
                FieldRowPanel([
                    FieldPanel('key'),
                    FieldPanel('value'),
                ]),
                InlinePanel(relation_name='fa_files',    panels=[ FieldPanel('file') ], heading="Files",),
                InlinePanel(relation_name='ea_entities', heading="Entities",),
            ])

class EntityViewSet(SnippetViewSet):
    model         = Entity
    icon          = 'thumbtack'
    list_display  = ("admin_thumb", "reference", "classification", "created_at")    
    list_filter   = ('classification',)
    ordering      = ["-created_at"]

    edit_handler = ObjectList([
                FieldPanel('avatar'),
                FieldPanel('classification'),
                FieldPanel('description'),
                InlinePanel(
                        relation_name='ea_attributes',
                        min_num=1,
                        heading="Attributes",
                        panels=[
                            FieldPanel('attribute'),
                        ],
                    ),
            ])
