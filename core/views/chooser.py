from wagtail.admin.widgets.chooser       import BaseChooser
from wagtail.admin.viewsets.chooser      import ChooserViewSet
from wagtail.admin.widgets               import AdminDateTimeInput
from wagtail.admin.forms.models          import WagtailAdminModelForm
from wagtailgeowidget.widgets            import LeafletField, GeocoderField
from wagtailgeowidget                    import geocoders

from ..models              import *

class AnnotationChooser(BaseChooser):
    template_name = "core/widgets/attribute_chooser_v2.html"


class AttributeChooserViewSet(ChooserViewSet):

    class AttributeChooser(AnnotationChooser):
        '''
        Custom chooser to display attributes nicely.
        - replaces default snippet icon with user-defined icon from AttributeName model
        - change how name/value fields are displayed   
        '''
        _icon = "snippet"

        def get_display_title(self, instance):
            self._icon = instance.key.icon
            return f"""<small><i>{instance.name}</i></small>
                        <br>
                        <small><b>{instance.value}</b></small>"""
            
        def get_context(self, name, value_data, attrs):
            ctx = super().get_context(name, value_data, attrs)
            ctx['icon'] = self._icon
            return ctx
    
    model       = Attribute
    form_fields = [ 'key', 'value' ]
    base_widget_class = AttributeChooser


attribute_chooser = AttributeChooserViewSet('attribute_chooser', url_prefix='attribute-chooser')
attribute_widget  = attribute_chooser.widget_class


class EntityClassificationChooserViewSet(ChooserViewSet):
    class EntityClassificationChooser(BaseChooser):
        template_name = "core/widgets/custom_chooser.html"
        def get_display_title(self, instance):
            return f"""
                <div class="col1">
                    {instance.get_admin_thumb_icon(width=30) }
                </div>
                <div class="col3">
                    {instance.name}
                </div>
            """
    model = EntityClassification
    model_name  = "Class"
    form_fields = [ 'name', 'icon' ]
    base_widget_class = EntityClassificationChooser

entity_classification_chooser = EntityClassificationChooserViewSet('entity_classification_chooser', url_prefix='entity-classification-chooser')
entity_classification_widget  = entity_classification_chooser.widget_class


class FileChooserViewSet(ChooserViewSet):
    icon       = 'folder-open-inverse'
    model      = File
    page_title = "Choose a File Source"
    per_page   = 10
    order_by   = '-created_at'

file_chooser_viewset = FileChooserViewSet('file_chooser', url_prefix='file-chooser')
file_chooser_widget  = file_chooser_viewset.widget_class


class OriginUrlChooserViewSet(ChooserViewSet):
    icon        = 'info-circle'
    model       = OriginUrl
    page_title  = "Choose Origin Url"
    per_page    = 10
    fields      = ['url', ]
    form_fields = ['url', ]

origin_url_chooser_viewset = OriginUrlChooserViewSet("origin_url_chooser")
origin_url_chooser_widget  = origin_url_chooser_viewset.widget_class


class EventChooserViewSet(ChooserViewSet):

    class EventChooser(AnnotationChooser):
        def get_display_title(self, instance):
            return f"""<small><i>Title</i></small>
                    <br>
                    <small><b>{instance.title}</b></small>"""

    class EventChooserCreateForm(WagtailAdminModelForm):
        class Meta:
            model = Event
            fields = ['title', 'start', 'end', 'exact']
            widgets = {
                'start': AdminDateTimeInput,
                'end':   AdminDateTimeInput,
                'exact': AdminDateTimeInput,
            }

    icon        = 'code'
    model       = Event
    page_title  = "Choose Event"
    per_page    = 10
    creation_form_class = EventChooserCreateForm
    base_widget_class   = EventChooser

event_chooser_viewset = EventChooserViewSet("event_chooser")
event_chooser_widget  = event_chooser_viewset.widget_class

class LocationChooserViewSet(ChooserViewSet):

    class LocationChooser(AnnotationChooser):
        def get_display_title(self, instance):
            return f"""<small><i>Address</i></small><br><small><b>{instance.address}</b></small>"""

    class LocationChooserCreateForm(WagtailAdminModelForm):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['address'].widget  = GeocoderField(geocoder=geocoders.NOMINATIM)
        class Meta:
            model = Location
            fields = ['address',]
            
    icon        = 'location'
    model       = Location
    page_title  = "Choose Location"
    per_page    = 10
    creation_form_class = LocationChooserCreateForm
    base_widget_class = LocationChooser

location_chooser_viewset = LocationChooserViewSet("location_chooser")
location_chooser_widget  = location_chooser_viewset.widget_class

from wagtail.admin.views.generic.chooser import ChosenView

class WorkspaceChooserViewSet(ChooserViewSet):

    class WorkspaceChosenView(ChosenView):
        def get_display_title(self, instance):
            return instance.name

    icon  = "folder-open-1"
    model = Workspace
    model_name  = "Class"
    chosen_view_class = WorkspaceChosenView

workspace_chooser = WorkspaceChooserViewSet('workspace_chooser', url_prefix='workspace-chooser')
workspace_widget  = workspace_chooser.widget_class
