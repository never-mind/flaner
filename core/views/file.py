from django.contrib.admin.utils import quote
from django                     import forms
from django.urls                import reverse
from wagtail.admin.filters      import django_filters, WagtailFilterSet
from wagtail.snippets.views     import snippets
from wagtail.admin.ui.tables    import TitleColumn
from wagtail.snippets.widgets   import SnippetListingButton
from wagtail.admin.panels       import InlinePanel, FieldPanel, ObjectList, TabbedInterface
from wagtail.admin.ui.tables    import Column, TitleColumn, BulkActionsCheckboxColumn

from ..panels  import WorkspacePanel
from ..models  import File, Workspace, WorkspaceChoiceField
from ..panels  import AnnotatePanel
from .chooser  import origin_url_chooser_widget


class FileFilterSet(WagtailFilterSet):
    """
    Dynamic filterset to limit user to filter only on workspaces 
    they have access to. 

    Custom File filter, extends WorkspaceFilterSet (which handles workspace
    filter) and adds mimetype filter for File Model listing page.

    """

    class WorkspaceChoiceFilterField(django_filters.ModelChoiceFilter):
        field_class = WorkspaceChoiceField

    def workspaces(request):
        if request is None:
            return Workspace.objects.none()
        else:
            return Workspace.objects.all()
            # WorkspaceMemberPermissionMixin()\
            #                 ._descendants_with_perm(
            #                     request.user, 
            #                     ["view", "add", "change", "delete"]
            #                 )
    workspace = WorkspaceChoiceFilterField(queryset=workspaces)

    def get_mimetype_choices():
        return [
            (i['mimetype'], i['mimetype']) for i in File.objects.order_by().values('mimetype').distinct()
        ]
    
    mimetype = django_filters.ChoiceFilter(choices=get_mimetype_choices,)

class FileIndexTitleColumn(TitleColumn):
    
    def __init__(self, listing_buttons, *args, **kwargs):
        self.listing_buttons = listing_buttons
        super().__init__(*args, **kwargs)
        
    def get_cell_context_data(self, instance, parent_context):
        context = super().get_cell_context_data(instance, parent_context)
        context["listing_buttons"] = self.listing_buttons(instance)
        return context

    cell_template_name = "core/snippets/tables/title_cell.html"


class FileIndexView(snippets.IndexView):
    
    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)

    def _get_listing_buttons(self, instance):
        if self.user_has_permission('change'):
            yield SnippetListingButton('edit',     self.get_edit_url(instance), classes={""})
        if self.user_has_permission('delete'):
            yield SnippetListingButton('delete',   self.get_delete_url(instance), classes={"no"})

    def get_edit_url(self, instance):
        return reverse(f'wagtailsnippets_{instance._meta.app_label}_{instance._meta.model_name}:edit', args=(quote(instance.pk),))

    def get_delete_url(self, instance):
        return reverse(f'wagtailsnippets_{instance._meta.app_label}_{instance._meta.model_name}:delete', args=(quote(instance.pk),))

    def get_columns(self):
        return [
                BulkActionsCheckboxColumn("checkbox", accessor=lambda obj: obj),

                FileIndexTitleColumn(
                    self._get_listing_buttons, 
                    'title',
                    width=600,
                    get_url=self.get_edit_url,
                    sort_key="title",
                ),
                Column('workspace',       width=50, sort_key="workspace",),
                Column('mimetype',        width=50, sort_key="mimetype",), 
                Column('origin_url_name', label='Origin',  width=50), 
                Column('created_at',      width=100, sort_key="created_at",),
                Column('admin_thumb', label='Thumb', width=50), 
                Column('annotation_sum', label='Annotations', width=50), 
            ]



class AnnotationInlinePanel(InlinePanel):

    def __init__(self, relation_name, annotation_field_name, fragment_field_name, *args, **kwargs):
        super().__init__(relation_name, *args,  **kwargs)
        self.annotation_field_name = annotation_field_name
        self.fragment_field_name   = fragment_field_name
    
    def clone_kwargs(self):
        kwargs = super().clone_kwargs()
        kwargs.update(
            annotation_field_name=self.annotation_field_name,
            fragment_field_name=self.fragment_field_name,
        )
        return kwargs

    class BoundPanel(InlinePanel.BoundPanel):
        template_name = "core/panels/inline_panel.html"

        def get_context_data(self, parent_context=None):
            context = super().get_context_data(parent_context)
            context["annotation_field_name"] = self.panel.annotation_field_name
            context["fragment_field_name"] = self.panel.fragment_field_name
            return context

FragmentPanel = FieldPanel('fragment',widget=forms.HiddenInput(attrs={
                                                'x-ref': "fragment_input",
                                                'x-data': """{
                                                    init() {
                                                        /* Hide fragment title */
                                                        Array.from(document.querySelectorAll('label[class="w-field__label"]'))
                                                            .filter( el => el.textContent.trim() === 'Fragment')
                                                            .forEach( el => $(el.parentNode).hide() );

                                                        /* Hide attribute/event/location title */
                                                        Array.from(document.querySelectorAll('label[class="w-field__label"]'))
                                                            .filter( el => ['Attribute*', 'Event*', 'Location*'].includes(el.textContent.trim()) )
                                                            .forEach( el => $(el).hide() );
                                                    }
                                                }"""
                                            }))


class FileViewSet(snippets.SnippetViewSet):
    """
    
    File Snippet Viewset 

    """
    model             = File
    icon              = 'doc-empty-inverse'
    menu_order        = 2
    ordering          = ["-created_at"]
    add_to_admin_menu = True

    index_view_class    = FileIndexView
    filterset_class     = FileFilterSet

    def __init__(self, model=None, **kwargs):
        super().__init__(model=model, **kwargs)
        self._create_handler = ObjectList([
                        FieldPanel('title'), 
                        WorkspacePanel('workspace'),
                        FieldPanel('file'),
                        FieldPanel('description'),
                        FieldPanel('origin_url'),
                    ]).bind_to_model(self.model)

    def get_form_class(self, for_update=False):
        if for_update:
            return self._edit_handler.get_form_class()
        else:
            return self._create_handler.get_form_class()

    # @property
    # def permission_policy(self):
    #     return FilePermissionPolicy(self.model)

    @property
    def add_view(self):
        return self.add_view_class.as_view(
            model=self.model,
            template_name=self.get_create_template(),
            header_icon=self.icon,
            permission_policy=self.permission_policy,
            panel=self._create_handler,
            form_class=self.get_form_class(),
            index_url_name=self.get_url_name("list"),
            add_url_name=self.get_url_name("add"),
            edit_url_name=self.get_url_name("edit"),
            preview_url_name=self.get_url_name("preview_on_add"),
        )

    edit_handler = AnnotatePanel(
                    [
                        TabbedInterface([                            

                            ObjectList([
                                FieldPanel('title'), 
                                FieldPanel('description'),

                                AnnotationInlinePanel("fa_attributes", "attribute", "fragment",
                                    heading="Attributes",
                                    panels=[FieldPanel('attribute'), FragmentPanel]),

                                AnnotationInlinePanel("fe_events", "event", "fragment",
                                    heading="Events",
                                    panels=[ FieldPanel('event'), FragmentPanel]),

                                AnnotationInlinePanel("fl_locations", "location", "fragment",
                                    heading="Locations",
                                    panels=[ FieldPanel('location'), FragmentPanel]),

                            ], heading='Main'),

                            ObjectList([
                                WorkspacePanel('workspace'),
                                FieldPanel('file'),
                                FieldPanel('thumb'),
                                FieldPanel('origin_url', widget=origin_url_chooser_widget),
                                FieldPanel('mimetype'),
                                FieldPanel('presentations'),

                            ], heading='Details'),
                        ])

                    ], 
                )
