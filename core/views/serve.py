
from django.dispatch                      import Signal
from django.conf                          import settings
from django.shortcuts                     import get_object_or_404
from django.http                          import HttpResponse
from django.views.decorators.clickjacking import xframe_options_sameorigin
from django.views.decorators.http         import etag
from django.core.exceptions               import PermissionDenied
from django.template                      import loader
from django.urls                          import reverse
from wagtail.utils.sendfile               import sendfile
from wagtail.utils                        import sendfile_streaming_backend

from ..models  import File

def file_etag(request, file_id, **kwargs):
    if hasattr(File, 'file_hash'):
        return File.objects.filter(id=file_id).values_list('file_hash', flat=True).first()

file_served = Signal()

@xframe_options_sameorigin
@etag(file_etag)
def file_serve(request, file_id, serve_thumbs=False):

    user = request.user

    if user.is_anonymous or not user.has_perms(["wagtailadmin.access_admin"]):
        raise PermissionDenied

    file = get_object_or_404(File, id=file_id)

    # Send file_served signal
    file_served.send(sender=File, instance=file, request=request)
    
    sendfile_opts = {}
    if not hasattr(settings, "SENDFILE_BACKEND"):
        # Fallback to streaming backend if user hasn't specified SENDFILE_BACKEND
        sendfile_opts["backend"] = sendfile_streaming_backend.sendfile

    # Choose between thumbs and actual files
    if serve_thumbs:
        
        # sendfile_opts["attachment"] = True
        # sendfile_opts["attachment_filename"] = file.thumb.name
        # sendfile_opts["mimetype"] = 'image/png'
        
        path = file.thumb.path

    else:
        # Use wagtail.utils.sendfile to serve the file;
        # this provides support for mimetypes, if-modified-since and django-sendfile backends
        
        sendfile_opts["attachment"] = (file.content_disposition != "inline")
        sendfile_opts["attachment_filename"] = file.filename
        sendfile_opts["mimetype"] = file.content_type
        
        path = file.file.path

    return sendfile(request, path, **sendfile_opts)



@xframe_options_sameorigin
# @require_wagtail_login
def file_preview(request, file_id):
    file = get_object_or_404(File, id=file_id)

    template = loader.get_template('core/preview/iframe.html')
    context = {
        "file": file,
        "file_url": reverse('file_serve', kwargs={"file_id": file.id}),
        "preview_mimetypes": settings.PREVIEW_MIMETYPES,
    }    
    return HttpResponse(template.render(context, request))