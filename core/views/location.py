from wagtail.snippets.views.snippets import SnippetViewSet
from wagtail.admin.panels            import *
from wagtailgeowidget                import geocoders
from wagtailgeowidget.panels         import GeoAddressPanel, LeafletPanel

from ..models import Location

class LocationViewSet(SnippetViewSet):
    icon          = 'site'
    model         = Location
    list_display  = ("address", "location")

    edit_handler = ObjectList([
        GeoAddressPanel("address", geocoder=geocoders.NOMINATIM),
        FieldPanel('zoom'),
        LeafletPanel("location", 
                address_field="address",
                zoom_field="zoom",
            ),
        FieldPanel('description'),
        InlinePanel('fl_files', panels=[FieldPanel('file'),], heading="Files"),
    ])