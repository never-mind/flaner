from django      import forms 
from django.urls import path
from wagtail.admin.viewsets.model    import ModelViewSet
from wagtail.admin.views             import generic
from wagtail.snippets.views.snippets import SnippetViewSet
from wagtail.admin.panels            import FieldPanel, ObjectList

from ..models import Workspace, WorkspaceChoiceField, get_root_workspace_id

from wagtail.admin.forms.models import WagtailAdminModelForm

class WorkspaceAdminForm(WagtailAdminModelForm):
    '''
    Custom Workspace admin form to create/edit workspace tree.

    Workspace use MP_Node(django-treebread Materialized path model) that needs special care to keep the tree in clean state.
    
    The form uses the `parent` field, which is not a real workspace field, to correctly manage the model:

    1. Defines the default field value in form initialization to ROOT workspace.
    2. On new instances attach the instance to `parent`
    3. On existing instances moves the instance (and its children) to new parent.     

    '''
    parent = WorkspaceChoiceField(
        label="Parent",
        queryset=Workspace.objects.all(),
        initial=get_root_workspace_id,
        required=True,
        help_text="Select hierarchical position. Note: a workspace cannot become a child of itself or one of its descendants."
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.is_root():
            self.fields['parent'].disabled = True
            self.fields['parent'].required = False
            self.fields['parent'].widget = forms.HiddenInput()
        elif self.instance.id:
            self.fields['parent'].initial = self.instance.get_parent() or get_root_workspace_id

    class Meta:
        model = Workspace
        fields = ("name", "description", )

    def save(self, commit=True):
        instance = super().save(commit=False)
        parent = self.cleaned_data["parent"]
        if not commit:
            return instance
        if instance.get_parent() == None: # creating a new node
            instance = parent.add_child(instance=instance)
        else:
            instance.save()
            if instance.get_parent() != parent:
                instance.move(parent, pos='sorted-child')
        return instance


class WorkspaceViewSet(SnippetViewSet):
    add_to_admin_menu = True
    menu_order   = 1
    icon  = "folder-open-1"
    model = Workspace
    edit_handler = ObjectList([
        FieldPanel('name'),
        FieldPanel('description'),
        FieldPanel('parent'),
        
    ])

    def get_form_class(self, for_update=False):
        return WorkspaceAdminForm
    
    def get_queryset(self, request):
        """
        In listing view returns all workspaces except ROOT workspace.
        """
        return self.model.objects.exclude(depth=1).all()

    list_display = ['indented_name', 'description',]
