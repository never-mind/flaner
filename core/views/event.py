from wagtail.snippets.views.snippets import SnippetViewSet
from wagtail.admin.panels            import *

from ..models import Event

class EventViewSet(SnippetViewSet):
    icon          = 'time' # 'history'
    model         = Event
    list_display  = ("title",)

    edit_handler = ObjectList([
        FieldPanel('title'),
        FieldPanel('exact'),
        FieldRowPanel([
            FieldPanel('start'),
            FieldPanel('end'),
        ]),
        FieldPanel('description'),
        InlinePanel('fe_files', panels=[FieldPanel('file'),], heading="Files"),
    ])
