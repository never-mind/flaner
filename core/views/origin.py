from wagtail.snippets.views     import snippets
from wagtail.admin.panels       import InlinePanel, FieldPanel, ObjectList

from ..models  import OriginUrl
from ..panels  import WorkspacePanel, FileLinkPanel

class OriginUrlViewSet(snippets.SnippetViewSet):
    icon              = "link"
    model             = OriginUrl
    add_to_admin_menu = True
    menu_order        = 6
    list_display      = [ 'url', 'workspace', 'download_type' ]

    def __init__(self, model=None, **kwargs):
        super().__init__(model=model, **kwargs)
        self._create_handler = ObjectList([
                        FieldPanel('url'),
                        WorkspacePanel('workspace',),
                        FieldPanel('download_type'),
                        FieldPanel('force_download'),
                        InlinePanel('files', panels=[], min_num=0, max_num=0,)
                    ]).bind_to_model(self.model)

    def get_form_class(self, for_update=False):
        if for_update:
            return self._edit_handler.get_form_class()
        else:
            return self._create_handler.get_form_class()

    edit_handler = ObjectList([
        FieldPanel('url'),
        WorkspacePanel('workspace',),
        FieldPanel('download_type'),
        FieldPanel('force_download'),
        InlinePanel("files", 
                    panels=[ 
                        FileLinkPanel(),
                    ],
                    max_num=1,
                    heading='Files',),
    ])
