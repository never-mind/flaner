from wagtail.snippets.views.snippets import SnippetViewSetGroup

from .entity   import EntityViewSet, AttributeViewSet, AttributeNameViewSet, EntityClassificationViewSet
from .event    import EventViewSet
from .location import LocationViewSet 

class AnnotationViewSetGroup(SnippetViewSetGroup):
    items      = (
        AttributeViewSet,
        EventViewSet,
        LocationViewSet,
        EntityViewSet,
        AttributeNameViewSet,
        EntityClassificationViewSet
    )
    menu_icon  = "thumbtack"
    menu_label = "Annotations"
    menu_name  = "Annotations"
    menu_order = 3
    add_to_admin_menu = True
