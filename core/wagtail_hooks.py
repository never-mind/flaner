from wagtail                         import hooks
from wagtail.snippets.models         import register_snippet, register_snippet_viewset
from wagtail.admin.menu              import MenuItem

from .models   import *
from .views    import *

# Registered at admin main menu
register_snippet(AnnotationViewSetGroup)
register_snippet(Workspace, viewset=WorkspaceViewSet)
register_snippet(File,        viewset=FileViewSet)
register_snippet(OriginUrl,   viewset=OriginUrlViewSet)
register_snippet(Presentation,        viewset=PresentationViewSet)

# Choosers

@hooks.register('register_admin_viewset')
def register_attribute_chooser():
    return attribute_chooser

@hooks.register('register_admin_viewset')
def register_entity_classification_chooser_viewset():
    return entity_classification_chooser

@hooks.register('register_admin_viewset')
def register_file_chooser_viewset():
    return file_chooser_viewset

@hooks.register('register_admin_viewset')
def register_origin_url_chooser_viewset():
    return origin_url_chooser_viewset

@hooks.register('register_admin_viewset')
def register_event_chooser_viewset():
    return event_chooser_viewset

@hooks.register('register_admin_viewset')
def register_location_chooser_viewset():
    return location_chooser_viewset

@hooks.register('register_admin_viewset')
def register_workspace_chooser_viewset():
    return workspace_chooser
