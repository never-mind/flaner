
from django.contrib.auth.models         import Permission
from django.contrib.contenttypes.models import ContentType
from django.forms.models        import ModelChoiceIterator, ModelChoiceIteratorValue
from wagtail.admin.panels       import FieldPanel

# from ..views.viewsets import WorkspaceMemberPermissionMixin

class WorkspaceChoiceIterator(
            # WorkspaceMemberPermissionMixin,
            ModelChoiceIterator):
    
    def __init__(self, request_user, field):
        self.field = field
        self._indentation_start_depth = 2
        # self.queryset = field.queryset.filter(
        #                     id__in=self._descendants_with_perm(
        #                         request_user, 
        #                         ["add", "change", "delete"]
        #                     )
        #                 )

    def choice(self, obj):
        return (
            ModelChoiceIteratorValue(self.field.prepare_value(obj), obj),
            obj.get_indented_name( self._indentation_start_depth,  self.field.label_from_instance(obj) ),
        )
    

class WorkspacePanel(FieldPanel):
    class BoundPanel(FieldPanel.BoundPanel):
        def __init__(self, **kwargs):
            super().__init__(**kwargs)
            self.form.fields[self.field_name].widget.choices = WorkspaceChoiceIterator(
                                                                    self.request.user,
                                                                    self.form.fields[self.field_name])
