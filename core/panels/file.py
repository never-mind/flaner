from wagtail.admin.panels    import HelpPanel
from django.conf             import settings
from django.urls             import reverse

class FilePreviewPanel(HelpPanel):

    def __init__(
            self,
            template="core/panels/file_preview.html",
            **kwargs,
        ):
        super().__init__(**kwargs)
        self.content  = ""
        self.template = template

    class BoundPanel(HelpPanel.BoundPanel):

        def get_context_data(self, parent_context=None):
            context = super().get_context_data(parent_context)
            context["file"] = self.instance
            context["file_url"] = reverse('file_serve', kwargs={"file_id": self.instance.id})
            context["preview_mimetypes"] = settings.PREVIEW_MIMETYPES
            return context


class FileLinkPanel(HelpPanel):

    def __init__(
            self,
            template="core/panels/file_link.html",
            **kwargs,
        ):
        super().__init__(**kwargs)
        self.content  = ""
        self.template = template

    class BoundPanel(HelpPanel.BoundPanel):

        def get_context_data(self, parent_context=None):
            context = super().get_context_data(parent_context)
            context["file"] = self.instance
            context["file_url"] = self.instance.admin_url
            context["file_title"] = self.instance.title
            return context