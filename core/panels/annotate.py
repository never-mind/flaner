from django.forms      import Media
from django.utils.html import html_safe
from django.urls       import reverse
from wagtail.admin.ui.components import Component
from wagtail.admin.panels import PanelGroup, InlinePanel, MultiFieldPanel
from django.utils.functional      import cached_property

@html_safe
class DeferedAlpinejs:
    '''
    Following this [recipe](https://docs.djangoproject.com/en/4.2/topics/forms/media/#paths-as-objects)
    to add 'defer' attribute to <script> tag on django Media class.
    
    'defer' attribute is required by alpine.js
    '''
    def __str__(self):
        return '<script defer src="/static/core/alpinejs@3.13.0.min.js"></script>'


class BasePreview(Component):

    def get_context_data(self, parent_context):
        context = super().get_context_data(parent_context)
        return context
    
    class Media:
        js = [ 
            DeferedAlpinejs(), 
            # '/static/core/alpinejs@3.13.0.min.js',
            # '/static/core/d3.v7.min.js',
        ]

class PdfPreview(BasePreview):
    template_name = 'core/panels/components/pdf.html'
    class Media:
        js = [ '/static/core/pdfjs-3.10.111/build/pdf.js',]

class ImagePreview(BasePreview):    
    template_name = 'core/panels/components/image.html'

class VideoPreview(BasePreview):
    template_name = 'core/panels/components/video.html'

class TextPreview(BasePreview):
    template_name = 'core/panels/components/text.html'


class AnnotatePanel(PanelGroup):

    """
    AnnotatePanel is a two-side view where users can preview the file on left side and add annotations on the right side.
    
    It is used by FileViewSet.edit_handler (from 'core/views/file.py'), to render the edit file form.
    
    It mostly adds client-side javascript functionality to render different file types (pdf, video, image, ...) on screen 
    and add annotations on them. We make use of alpinejs where we direct different html elements to interact with each other,
    and we don't use or alter any of wagtail's javascript code. 
    
    The template (core/panels/annotate.html) splits the UI in left and right containers, where
    
    + on left side users can preview the file
    + on right side users can edit the file information and add annotations

    On the **left side**, the template will render a list of `panels` provided by django. Those panels (which use the "Preview" components from above)
    are responsible to render the file, and add some controls where user can interact with the file ( e.g: change pdf page, play/stop video, ...).

    The AnnotatePanel decides which `panels` to render based on `File.mimetype` (see here: `AnnotatePanel.BoundPanel.get_components_from_mimetype` )
    The `panels` contain different layers, html elements that each one sit on top of the others.
    For example a pdf file will use the `PdfPreview` panel which renders a html5 canvas layer ( as seen here 'templates/core/panels/components/pdf_canvas.html' ).
    All different panels add an extra common layer - the control layer - as defined here: 'templates/core/panels/components/control_canvas.html'.

    On the **right side**, the template will render a list of File fields. The fields can be seen in `FileViewSet.edit_handler` 
    located at 'core/views/file.py'. A custom AnnotationInlinePanel, amonsts other fields, is used to display the relational
    attribute, event, location fields and handle user interactions related to annotations. 


    In summary, following are the different pieces that all together combine the form.

    - 'core/views/file.py' => FileViewSet
    - 'core/panels/annotate.py' => AnnotatePanel
        - 'core/templates/core/panels/annotate.html'
        - panels:
            - 'core/panels/components/pdf.html'
            - 'core/panels/components/pdf_canvas.html'
            - 'core/panels/components/image.html'
            - 'core/panels/components/video.html'
            - 'core/panels/components/text.html'
            - 'core/panels/components/control_canvas.html'
    - 'core/views/file.py' => AnnotationInlinePanel
        - 'core/templates/core/panels/inline_panel.html' 
        - 'core/templates/core/panels/inline_panel_child.html' 
    - 'core/views/chooser.py' => AnnotationChooser
        - 'core/templates/core/widgets/attribute_chooser_v2.html'
    """

    class BoundPanel(PanelGroup.BoundPanel):

        template_name = "core/panels/annotate.html"

        def get_components_from_mimetype(self):
            '''
            Check the file mimetype and return the appropriate Component 
            '''
            if self.instance.mimetype:
                if self.instance.mimetype == 'application/pdf':
                    yield PdfPreview()
                elif self.instance.mimetype.startswith('image/'):
                    yield ImagePreview()
                elif self.instance.mimetype.startswith('video/'):
                    yield VideoPreview()
                elif self.instance.mimetype.startswith('text/'):
                    yield TextPreview()

        def get_canvas_context(self, parent_context={}):
            return dict(
                scale=1.0,
                current_time=1.5,
            )

        @property
        def media(self):
            '''
            Load Media classes
            '''
            media = super().media + Media(css=dict(all=['/static/core/panels/annotate.css']))
            for component in self.get_components_from_mimetype():
                media += component.media
            return media
    
        def get_context_data(self, parent_context=None):
            context = super().get_context_data(parent_context)
            context['annotate_panel_context'] = self.get_canvas_context(parent_context)
            context['annotate_panel_context']["file_url"] = self.request.build_absolute_uri( reverse('file_serve', kwargs={"file_id": self.instance.id}) )
            context['panels']   = list(self.get_components_from_mimetype())
            return context
