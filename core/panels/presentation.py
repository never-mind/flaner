from django.utils.functional import cached_property
from wagtail.admin.panels    import MultiFieldPanel, HelpPanel, PanelGroup
from django.forms            import Media
import json

from core.models import PresentationType
from django.utils.html import format_html

class PresentationPanel(HelpPanel):
    def __init__(
            self,
            template="core/panels/presentation.html",
            **kwargs,
        ):
        super().__init__(**kwargs)
        self.content  = ""
        self.template = template
    
    class BoundPanel(HelpPanel.BoundPanel):
        
        def get_context_data(self, parent_context=None):
            context = super().get_context_data(parent_context)
            context['username'] = self.request.user.username
            context['presentation_type'] = self.instance.presentation_type
            return context

           
        @property
        def media(self):
            # Graph Additional media
            if self.instance.presentation_type == PresentationType.graph:
                return Media(
                        css=dict(
                            all=[
                                '/static/panels/presentation/presentation.css',
                            ]
                        ),
                        js=[
                            '/static/panels/presentation/cytoscape.min.js',
                            '/static/panels/presentation/cytoscape-node-html-label.min.js',
                            '/static/panels/presentation/graph.js',
                        ]
                    )
            # Map Additional media
            elif self.instance.presentation_type == PresentationType.map:
                return Media(
                        css=dict(
                            all=[
                                '/static/panels/presentation/leaflet.css',
                                '/static/panels/presentation/presentation.css',
                            ]
                        ),
                        js=[
                            '/static/panels/presentation/leaflet.js',
                            '/static/panels/presentation/map.js',
                        ]
                    )
            # Timeline Additional media
            elif self.instance.presentation_type == PresentationType.timeline:
                return Media(
                        css=dict(
                            all=[
                                '/static/panels/presentation/timeline.css',
                                '/static/panels/presentation/presentation.css',
                            ]
                        ),
                        js=[
                            '/static/panels/presentation/timeline.js',
                            '/static/panels/presentation/timeline-loader.js',
                        ]
                    )

        @property
        def map_json_data(self):
            '''
            Return all linked locations as json serialized object 
            '''
            locations = []
            for loc in self.instance.locations.all():
                if loc.content_object.location:
                    loc_dict = dict(
                        address=loc.content_object.address,
                        lat=float(loc.content_object.lat),
                        lng=float(loc.content_object.lng),
                        zoom=loc.content_object.zoom,
                        description=loc.content_object.description,
                        url=loc.content_object.admin_url,
                        files=[
                            dict(
                                url=fl.file.admin_url,
                                title=fl.file.title,
                                workspace=fl.file.workspace.name,
                            ) for fl in loc.content_object.fl_files.all() 
                        ]
                    )
                    locations.append(loc_dict)

            return json.dumps(dict(locations=locations))

        @property
        def timeline_json_data(self):
            events=[]
            for e in self.instance.events.all():
                event = {}
                
                if e.content_object.exact:
                    event["start_date"] = {
                        "month": str(e.content_object.exact.month),
                        "day":   str(e.content_object.exact.day),
                        "year":  str(e.content_object.exact.year),
                    }
                elif e.content_object.start and e.content_object.end:
                    event["start_date"] = {
                        "month": str(e.content_object.start.month),
                        "day":   str(e.content_object.start.day),
                        "year":  str(e.content_object.start.year),
                    }
                    event["end_date"] = {
                        "month": str(e.content_object.end.month),
                        "day":   str(e.content_object.end.day),
                        "year":  str(e.content_object.end.year),
                    }
                else:
                    continue

                event["text"] = {
                    "headline": e.content_object.title,
                    "text":  f'''
                    
                    <b>Description</b>
                    <p>{e.content_object.description}</p> 
                    <br>

                    <b>Files</b>
                    <ul>
                    {
                        ''.join([
                            f"<li><a href={fe.file.admin_url}>{fe.file.title}</a> ({fe.file.workspace.name})</li>" for fe in e.content_object.fe_files.all()
                        ])
                    }
                    </ul>
                    '''
                }

                events.append(event)
            
            return json.dumps(dict(events=events))

        @property
        def graph_json_data(self):
            '''
            TODO: 
            1. Add Layout configuration
            2. Itergrate cytoscape-elk  layout https://cytoscape.org/cytoscape.js-elk/?demo=mrtree
            2. Itergrate cytoscape-klay layout https://cytoscape.org/cytoscape.js-klay/


            '''
            styles   = []
            elements = dict(nodes=[], edges=[],)
            for i in self.build_graph_data:
                tp = i.pop('type')
                if tp == 'node':
                    elements['nodes'].append(i)
                elif tp == 'edge':
                    elements['edges'].append(i)
                elif tp == 'style':
                    styles.append(i)
            return json.dumps(dict(elements=elements, styles=styles))


        @property
        def build_graph_data(self):
            
            # Node Styles
            yield dict(
                type='style',
                selector='node',
                style={
                    
                    'background-color': '#5C5C5C',
                    'background-fit': 'cover',
                    'font-family':  "FreeSet,Arial,sans-serif",
                    "font-size": 10,
                })

            yield dict(
                type='style',
                selector='node[category="file"]',
                style={
                    "shape": 'round-rectangle',
                    'width': '200 * data(width)',    
                })
                

            # Edge Styles
            yield dict(
                    type='style',
                    selector='edge',
                    style={
                        # "label": "data(label)",
                        "width": 1,
                        'line-color':  '#5C5C5C',

                        "target-arrow-shape": "triangle",
                        'target-arrow-color':  '#5C5C5C',
                        
                        "curve-style": "straight",
                        # "curve-style": "unbundled-bezier",
                        # "control-point-distance": 30,
                        # "control-point-weight": 0.5,

                        "opacity": 0.9,
                        "overlay-padding": "3px",
                        "overlay-opacity": 0,

                        # "font-family": "FreeSet,Arial,sans-serif",
                        # "font-size": 9,
                        # "font-weight": "bold",
                        # "text-background-opacity": 1,
                        # "text-background-color": "#ffffff",
                        # "text-background-padding": 3,
                        # "text-background-shape": "roundrectangle",
                        
                    })


            # for e in self.instance.mark.entities.all():
            #     for ea in e.content_object.ea_attributes.all():
            #         yield dict(
            #                 type='node',
            #                 data=dict(
            #                     id=f'a_{ea.attribute.id}',
            #                     label=f'{ea.attribute}',
            #                     width=len(f'{ea.attribute}'),
            #                     url='#',
            #                     category="attribute"))


            for fl in self.instance.files.all():

                for fa in fl.content_object.fa_attributes.all():
                    
                    # File
                    yield dict(
                            type='node',
                            data=dict(
                                id=f'f_{fa.file.id}', 
                                label=fa.file.title,
                                width=len(fa.file.title),
                                url=fa.file.admin_url,
                                category="file"))
                    
                    # File Style
                    yield dict(
                            type='style',
                            selector=f'#f_{fa.file.id}',
                            style={
                                'background-image': fa.file.thumb_url,
                            })
                    
                    # Attribute
                    yield dict(
                            type='node',
                            data=dict(
                                id=f'a_{fa.attribute.id}',
                                label=f'{fa.attribute}',
                                width=len(f'{fa.attribute}'),
                                url=fa.attribute.admin_url,
                                category="attribute"))

                    # File / Attribute Edge
                    yield dict(
                            type='edge',
                            data=dict(
                                    source=f'a_{fa.attribute.id}', 
                                    target=f'f_{fa.file.id}', 
                                    id=f'a{fa.id}', ))
