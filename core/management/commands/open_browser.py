import subprocess
from django.core.management.base import BaseCommand
from core.models import CoreSettings

class Command(BaseCommand):
    help = "Open Browser"

    def handle(self, *args, **options):
        bs = CoreSettings.load()
        # args = [ "chromium", ]
        # if self.proxy:
        #     args.append(f"--proxy-server={self.proxy}")
        # args.append(url)
        subprocess.run(bs.chromium_cli_base)
        