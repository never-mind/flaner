class Command(BaseCommand):
    help = "Search and downloads warc files"

    def add_arguments(self, parser):
        parser.add_argument("url", type=str)
        parser.add_argument("-w", "--workspace", default="ROOT")

    def handle(self, *args, **options):
        """
        Follow the article to search for given url in web archive,
        download archive, extract url and import it.
        
        https://itnext.io/get-cia-certificate-by-exploring-archives-3d5357c28193
        
        """
        pass