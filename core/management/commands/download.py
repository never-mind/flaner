import re
import tempfile
import filetype
import json
# import requests
import subprocess
import io
import urllib.parse

from django.core.files import File as DjangoFile
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.management.base import BaseCommand, CommandError

from core.models import *

def youtube_download(url, temp_name, temp_path, proxy=""):
    output = '/'.join([ temp_path, temp_name ])
    subprocess.run([
        'youtube-dl',
            f"--proxy={proxy}",
            f"--output={output}.%(ext)s",
            
            "--write-info-json",  # "--skip-download",
            "--write-annotations",
            "--write-thumbnail",

            "--format=mp4",
            # "--recode-video=mp4",

            "--write-sub",
            "--write-auto-sub",
            "--convert-subs=srt",
            url
    ])
    
    info  = json.loads( open(f"{output}.info.json").read() )
    
    del info['formats']
    fl = File()
    fl.title = info['fulltitle']
    fl.file  = DjangoFile(open(f"{output}.mp4", 'rb'), name=f"{temp_name}.mp4")
    try:
        fl.thumb = DjangoFile(open(f"{output}.jpg", 'rb'), name=f"{temp_name}.thumb.jpg")        
    except:
        pass
    # fl.meta = info
    return fl


def default_download(url, temp_name, temp_path, proxy=""):
    output = '/'.join([ temp_path, temp_name ])

    fl = File()

    head = requests.head(url)
    
    fl.mimetype = head.headers.get('Content-Type').split(';')[0]

    if fl.mimetype == "text/html":

        # Use playwright to dowload webpage as pdf
        # 
        fl.mimetype = 'application/pdf'

        from playwright.sync_api import sync_playwright
        with sync_playwright() as playwright:
            # browser = playwright.chromium.launch(headless=False)
            # browser = p.chromium.connect_over_cdp("http://localhost:9222")
            browser = playwright.chromium.launch_persistent_context(
                '/tmp/user_data_dir',
                args=[
                    "--headless=new", # the new headless arg
                    # f"--disable-extensions-except={pathToExtension}",
                    # f"--load-extension={pathToExtension}",
                ],
                headless=False,
                # accept_downloads=True,
                # record_har_path="/tmp/har.json",
                # downloads_path='/tmp/downloads_path',
            )
            page = browser.pages[0]
            page.goto(url)        
            # page.wait_for_timeout(10000)
            
            title    = page.title()
            content  = io.BytesIO(page.pdf())
            filename = urllib.parse.unquote([ i for i in url.split('/') if i ][-1])
            
            # page.close()
            # browser.close()


        fl.title = title
        fl.file  = DjangoFile(content, name=f"{filename}.pdf")

    else:
        # Download any other file as is without modification
        #
        with requests.get(url, stream=True) as r:
            r.raise_for_status()
            chunk_size = 1024
            if r.headers.get('content-length'):
                if int(r.headers.get('content-length')) < chunk_size:
                    chunk_size = int(r.headers.get('content-length'))
            with open(output, 'wb') as f:
                for chunk in r.iter_content(chunk_size=chunk_size):
                    # If you have chunk encoded response uncomment if
                    # and set chunk_size parameter to None.
                    #if chunk: 
                    f.write(chunk)
    
        # Generate title
        if fl.mimetype and filetype.get_type(fl.mimetype):
            ext   = filetype.get_type(fl.mimetype).EXTENSION
            title = f"{temp_name}.{ext}"
        else:
            title = temp_name
    
        fl.title = title
        fl.file  = DjangoFile(open(output, 'rb'), name=title)
    
    return fl

class Command(BaseCommand):
    help = "Downloads files from given urls"

    def add_arguments(self, parser):
        parser.add_argument("url", type=str)
        parser.add_argument("-w", "--workspace", default="ROOT")

    def handle(self, *args, **options):
        temp_name = next(tempfile._get_candidate_names())
        temp_path = tempfile._get_default_tempdir() 

        wsp = Workspace.objects.get(name=options['workspace'])

        try:

            if re.match('https://(www\.)?(youtube.com|youtu.be)', options["url"]):
                fl = youtube_download(options["url"], temp_name, temp_path, proxy="")
            else:
                fl = default_download(options["url"], temp_name, temp_path, proxy="")

        except Exception as e:
            self.stderr.write(self.style.ERROR('Could not download "%s"' % options['url']))
            self.stderr.write(self.style.ERROR(str(e)))


        else:                        
            origin = OriginUrl(url=options["url"], workspace_id=wsp.id)
            origin.save()

            fl.workspace_id  = wsp.id
            fl.origin_url_id = origin.id
            fl.save()

# pathToExtension="/home/red/.config/chromium/Default/Extensions/cjpalhdlnbpafiamejdnhcphjbkeiagm"
# browser = p.chromium.launch_persistent_context(
#     '/tmp/user_data_dir',
#     args=[
#         # "--headless=new", # the new headless arg for chrome v109+. Use '--headless=chrome' as arg for browsers v94-108.    
#         f"--disable-extensions-except={pathToExtension}",
#         f"--load-extension={pathToExtension}",
#     ],
#     headless=False,
#     accept_downloads=True,
#     record_har_path="/tmp/har.json",
#     downloads_path='/tmp/downloads_path',
# )
# # page = browser.new_page()
# page = browser.pages[0]
# page.on("download", lambda download: print(download.path()))
# page.goto(options["url"])
# # generates a pdf with "screen" media type.
# # page.emulate_media(media="screen")
# # page.pdf(path="page.pdf")
# # page.close()
# # page.wait_for_event("close", timeout=0)
# # browser.close()

# # https://smsresults.minedu.gov.gr/panelladikes_sms_2022_faq.pdf

# with page.expect_download() as download_info:
#     page.goto('https://smsresults.minedu.gov.gr/panelladikes_sms_2022_faq.pdf')
#     # self.stdout.write(
#     #     self.style.SUCCESS('Successfully closed poll "%s"' % poll_id)
#     # )
# browser = p.chromium.connect_over_cdp("http://localhost:9222")

# Run in Incognito mode so cokkies sessions and setting are not working.
# context = browser.new_context(    
#     record_har_path="/tmp/example.har", 
#     # record_har_url_filter="**/api/**"
# )
# page = context.new_page()

# default_context = browser.contexts[0]
# # page = default_context.pages[0]
# page = default_context.new_page()
# page.goto('https://www.documentonews.gr/article/kalos-irthate-stin-tailandi-tis-mesogeioy/')

# https://chrome.google.com/webstore/detail/webrecorder-archivewebpag/fpeoodllldobpkbkabpblcfaogecpndd



# WebRecorder
# 
# 1. User installs ArchiveWeb.page App 
#       https://github.com/webrecorder/archiveweb.page/releases/tag/v0.11.0
# 2. We implement the browsertrix-cloud api
#       https://github.com/webrecorder/browsertrix-cloud/blob/main/backend/btrixcloud/uploads.py#L238
# 3. User configures app's settings and point Browsertrix Cloud to our api.
# 4. Or downloads and uploads a collections manually


# Browsertrix Crawler
# 
# Additionally to WebRecorder we might use browsertrix-crawler to automate the crawling 
# of a url. https://github.com/webrecorder/browsertrix-crawler


# Serve Web Archive Files
#
# Intergrate web-replay-gen with Wagtail 
# https://github.com/webrecorder/web-replay-gen 