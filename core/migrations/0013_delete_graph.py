# Generated by Django 4.2.2 on 2023-09-07 00:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_mark_presentation_type'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Graph',
        ),
    ]
