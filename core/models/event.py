from django.db                  import models 
from django.core.exceptions     import ValidationError
from wagtail.admin.panels       import *
from wagtail.models             import ClusterableModel
from wagtail.search             import index
from .base import Base


class Event(ClusterableModel, index.Indexed, Base):
    title = models.CharField(max_length=500)
    exact = models.DateTimeField(null=True, blank=True)
    start = models.DateTimeField(null=True, blank=True)
    end   = models.DateTimeField(null=True, blank=True)
    description = models.TextField(null=True, blank=True, default='')

    def clean(self):
        if self.exact:
            if self.start or self.end:
                raise ValidationError({
                    'exact': 'When using exact field, `start` and `end` fields needs to be empty'
                })
        else:
            if not ( self.start and self.end ):
                raise ValidationError('You need to fill both `start` and `end` fields')

    def __str__(self):
        return self.title

    search_fields = [
        index.SearchField("title", boost=10),
        index.AutocompleteField("title"),
        index.AutocompleteField("description"),
    ]
