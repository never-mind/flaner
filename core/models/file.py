import sys
import filetype
from io  import BytesIO
from PIL import Image
from django.conf                    import settings
from django.db                      import models
from django.dispatch                import receiver
from django.db.models.signals       import post_delete
from django.urls                    import reverse
from django.contrib.admin.utils     import quote
from django.utils.functional        import cached_property
from django.utils.safestring        import mark_safe
from django.forms.utils             import flatatt
from django.core.validators         import FileExtensionValidator
from django.core.files.uploadedfile import InMemoryUploadedFile
from wagtail.search              import index
from wagtail.models              import Orderable, ReferenceIndex
from wagtail.documents.models    import AbstractDocument
from wagtail.api                 import APIField
from modelcluster.fields         import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.models         import ClusterableModel
from contextlib import contextmanager
from wagtail.utils.file import hash_filelike
import os.path
from mimetypes import guess_type
import urllib

from .base      import Base
from .workspace import WorkspaceMember


class File(WorkspaceMember, ClusterableModel, Orderable, 
           # AbstractDocument
           index.Indexed,
           Base
           ):
    """
    
    Custom File Model as a snippet with custom views and multi-table inheritance.

    File is a parent model generic enough for basic file handling requitements.
    However different file types have different requirements on parsing, viewing and 
    storing. Thus we use [multi-table inheritance](https://docs.djangoproject.com/en/3.2/topics/db/models/#multi-table-inheritance)
    to store different mimetypes to different tables, and add their own handling
    on those classes. 
    
    However for the index view we want to be able to search on the main instance,
    and the detail view on the subclass.
    
    List of fileds inherited:
        
        1. AbstractDocument
           - title
           - created_at
           - uploaded_by_user
           - tags
           - file_size
           - file_hash (SHA-1 hash)
        2. Orderable
           - sort_order
        3. CollectionMember
           - workspace

    """


    # Collection / Filesystem Map
    # 
    # It would be nice to map workspace tree to filesystem tree and upload files 
    # to the folder that map to relevant workspace. It would be very helpful in
    # case we want to expose our files through webdav or any other means.
    #
    # One issue here is that whenever a workspace is changed parent directory 
    # we should change the folder on filesystem and replace all files fields
    # in the database to map it.
    # Another issue is that if we want to move from many2one to many2many relation
    # then each file would belong to multiple workspaces thus myltiple folders.
    # A nice workaround would be to use softlinks to point to the actual file from
    # multiple paths but, that doesnt work with S3 like storages and I doubt it
    # will work with most webdav implementations.  
    # 
    # def upload_to_workspace(instance, filename):
    #     return instance.get_workspace_fullpath
    # 
    # file = models.FileField(upload_to=upload_to_workspace, ...
    # 
    file          = models.FileField(upload_to="uploads/%Y/%m/%d/", verbose_name="file", max_length=500, blank=True)
    thumb         = models.ImageField(upload_to="thumbs/%Y/%m/%d/", verbose_name="thumb", max_length=500, blank=True)
    mimetype      = models.CharField(max_length=255, blank=True, null=True)
    origin_url    = ParentalKey('core.OriginUrl', on_delete=models.SET_NULL, blank=True, null=True, related_name='files')
    presentations = ClusterTaggableManager(through='core.PresentationFile', verbose_name="presentations", blank=True)
    description   = models.TextField(null=True, blank=True, default='')

    title         = models.CharField(max_length=255, verbose_name="title")
    created_at    = models.DateTimeField(verbose_name="created at", auto_now_add=True)
    uploaded_by_user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name="uploaded by user",
        null=True,
        blank=True,
        editable=False,
        on_delete=models.SET_NULL,
    )
    uploaded_by_user.wagtail_reference_index_ignore = True

    file_size = models.PositiveIntegerField(null=True, editable=False)
    # A SHA-1 hash of the file contents
    file_hash = models.CharField(max_length=40, blank=True, editable=False)


    search_fields = [
        index.SearchField("title", boost=10),
        index.AutocompleteField("title"),
        index.AutocompleteField("description"),

        # index.RelatedFields(
        #     "tags",
        #     [
        #         index.SearchField("name", boost=10),
        #         index.AutocompleteField("name"),
        #     ],
        # ),
      
        index.RelatedFields('origin_url', [
            index.AutocompleteField('url'),
        ]),

        index.FilterField('mimetype'),
        index.FilterField('workspace_id'),
        index.FilterField("uploaded_by_user"),
    ]

    api_fields = [
        APIField('title'),
        APIField('mimetype'),
        APIField('workspace'),
        APIField('origin_url'),
        APIField('thumb'),
        APIField('file'),
        APIField('file_size'),
        APIField('file_hash'),
        APIField('created_at'),
        APIField('uploaded_by_user'),
        APIField('presentations'),
        APIField('description'),
    ]
    
    @cached_property
    def url(self):
        """
        Returns url to download file from.   
        """
        return reverse("file_serve", args=[str(self.id)])

    @cached_property
    def thumb_url(self):
        return reverse("file_thumb_serve", args=[str(self.id)])

    @property
    def file_text(self):
        """
        Returns file content in as string.
        """
        return self.file.read().decode('utf-8')
 
    def origin_url_name(obj):
        if obj.origin_url:
            _meta = obj.origin_url._meta
            return mark_safe(
                '''<a href={url}>link</a>''' .format(
                    url=reverse(f'wagtailsnippets_{_meta.app_label}_{_meta.model_name}:edit', args=(quote(obj.origin_url.pk),)), 
                )
            )
        return ""
    
    origin_url_name.short_description = 'Origin Url'
        
    @cached_property
    def content_type(self)->str:
        """
        Overwrite wagtail's implementation and serve content mimetype
        from database field.
        """
        return self.mimetype
    
    @cached_property
    def allow_embed(self)->bool:
        """
        Returns True is file is allowed to be embedded ( using iframe for example ) by
        checking if file's mimetype matches the ones defined in settings.WAGTAILDOCS_INLINE_CONTENT_TYPES.
        """
        if self.mimetype in getattr(settings, "WAGTAILDOCS_INLINE_CONTENT_TYPES", []):
            return True
        return False
    
    @cached_property
    def admin_thumb(self):
        """
        Return files thumbnail as html, ready to be used in templates.
        """
        if self.thumb:
            return mark_safe("<img {}>".format(flatatt({
                "src": self.thumb_url,
                "width": 50,
                "class": 'admin-thumb',
                "decoding": "async",
                "loading": "lazy",
            })))
        else:
            return mark_safe("""
                        <svg class="icon icon-doc-full " aria-hidden="true" width={width} height={width} color="{color}">
                            <use href="#icon-doc-full"></use>
                        </svg>""".format(width=24, color='lightgrey',))

    @property
    def get_workspace_paths(self):
        return [ c.name for c in self.workspace.get_ancestors() ] + [ self.workspace.name ]
    
    @property
    def get_workspace_fullpath(self):
        return '/'.join( self.get_workspace_paths[1:] + [self.filename] )

    @property
    def wsp_title(self):
        return '/'.join( self.get_workspace_paths[1:] + [self.title] )

    @property
    def annotation_sum(self):
        return self.fa_attributes.count() + self.fe_events.count() + self.fl_locations.count()    

    def guess_mimetype(self, force=False):
        """
        Guesses mimetype from the actual file content.
        """
        if not self.mimetype or force:
            self.mimetype = filetype.guess_mime(self.file)
            if not self.mimetype:
                if self.file_extension in ['yml', 'yaml', 'json', 'txt']:
                   self.mimetype = 'text/plain'

    def generate_thumbnail(self, thumb_size=(300, 300), force=False):
        """
        If file's mimetype is image then tries to generate a thumbnail using pillow 
        """
        if not self.thumb:
            if self.mimetype and self.mimetype.startswith('image'):
                try:
                    thumb = BytesIO()

                    img = Image.open(self.file)
                    
                    if img.height > thumb_size[0] or img.width > thumb_size[1]:
                        img.thumbnail(thumb_size)
                    
                    img.save(thumb, format='PNG')
                    
                except Exception as e:
                    pass
                else:
                    img_name = '.'.join(self.file.name.split('.')[:-1]) + '.thumb.png'
                    self.thumb = InMemoryUploadedFile(thumb,'ImageField', img_name, 'image/jpeg', sys.getsizeof(thumb), None)


    def clean(self):
        """
        Checks for WAGTAILDOCS_EXTENSIONS and validates the uploaded file
        based on allowed extensions that were specified.
        Warning : This doesn't always ensure that the uploaded file is valid
        as files can be renamed to have an extension no matter what
        data they contain.
        More info : https://docs.djangoproject.com/en/3.1/ref/validators/#fileextensionvalidator
        """
        allowed_extensions = getattr(settings, "WAGTAILDOCS_EXTENSIONS", None)
        if allowed_extensions:
            validate = FileExtensionValidator(allowed_extensions)
            validate(self.file)
        
        self.guess_mimetype()
        self.generate_thumbnail()

        # Set file metadata
        self._set_document_file_metadata()

    def save(self, *args, **kwargs):
        self.guess_mimetype()
        self.generate_thumbnail()
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = "file"
        verbose_name_plural = "files"
        indexes = [
            models.Index(fields=['mimetype',]),
        ]

    def is_stored_locally(self):
        """
        Returns True if the image is hosted on the local filesystem
        """
        try:
            self.file.path

            return True
        except NotImplementedError:
            return False

    @contextmanager
    def open_file(self):
        # Open file if it is closed
        close_file = False
        f = self.file

        if f.closed:
            # Reopen the file
            if self.is_stored_locally():
                f.open("rb")
            else:
                # Some external storage backends don't allow reopening
                # the file. Get a fresh file instance. #1397
                storage = self._meta.get_field("file").storage
                f = storage.open(f.name, "rb")

            close_file = True

        # Seek to beginning
        f.seek(0)

        try:
            yield f
        finally:
            if close_file:
                f.close()

    def get_file_size(self):
        if self.file_size is None:
            try:
                self.file_size = self.file.size
            except Exception:
                # File doesn't exist
                return

            self.save(update_fields=["file_size"])

        return self.file_size

    def _set_file_hash(self):
        with self.open_file() as f:
            self.file_hash = hash_filelike(f)

    def get_file_hash(self):
        if self.file_hash == "":
            self._set_file_hash()
            self.save(update_fields=["file_hash"])

        return self.file_hash

    def _set_document_file_metadata(self):
        self.file.open()

        # Set new document file size
        self.file_size = self.file.size

        # Set new document file hash
        self._set_file_hash()
        self.file.seek(0)


    @property
    def filename(self):
        return os.path.basename(self.file.name)

    @property
    def file_extension(self):
        return os.path.splitext(self.filename)[1][1:]

    @property
    def content_type(self):
        content_types_lookup = getattr(settings, "WAGTAILDOCS_CONTENT_TYPES", {})
        return (
            content_types_lookup.get(self.file_extension.lower())
            or guess_type(self.filename)[0]
            or "application/octet-stream"
        )

    @property
    def content_disposition(self):
        inline_content_types = getattr(
            settings, "WAGTAILDOCS_INLINE_CONTENT_TYPES", ["application/pdf"]
        )
        if self.content_type in inline_content_types:
            return "inline"
        else:
            return "attachment; filename={0}; filename*=UTF-8''{0}".format(
                urllib.parse.quote(self.filename)
            )

    def __str__(self):
        return self.title





@receiver(post_delete, sender=File)
def file_delete(sender, instance, **kwargs):
    """
    Receive the post_delete signal and delete the file associated with the model instance.
    Pass false so FileField doesn't save the model.
    """
    instance.file.delete(False)



class FileAttribute(index.Indexed, Orderable, Base):
    attribute = ParentalKey('core.Attribute', related_name="fa_files",      on_delete=models.CASCADE)
    file      = ParentalKey('core.File',      related_name="fa_attributes", on_delete=models.CASCADE)
    fragment  = models.JSONField(null=False, blank=True, default=dict)

class FileEvent(index.Indexed, Orderable, Base):
    event = ParentalKey('core.Event', related_name="fe_files",  on_delete=models.CASCADE)
    file  = ParentalKey('core.File',  related_name="fe_events", on_delete=models.CASCADE)
    fragment  = models.JSONField(null=False, blank=True, default=dict)

class FileLocation(index.Indexed, Orderable, Base):
    location = ParentalKey('core.Location', related_name="fl_files",     on_delete=models.CASCADE)
    file     = ParentalKey('core.File',     related_name="fl_locations", on_delete=models.CASCADE)
    fragment  = models.JSONField(null=False, blank=True, default=dict)