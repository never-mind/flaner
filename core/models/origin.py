
from django.db                       import models
from wagtail.models                  import Orderable
from wagtail.search                  import index
from modelcluster.models             import ClusterableModel
from wagtail.api                     import APIField

from .base      import Base
from .workspace import WorkspaceMember
from .file      import File
from .settings  import CoreSettings

class DownloadType(models.TextChoices):
    File              = 'F',  'File'
    # WebpagePdf        = 'WP', 'Webpage as Pdf'

class OriginUrl(index.Indexed, Orderable, WorkspaceMember, ClusterableModel, Base):

    url              = models.URLField(max_length=500)
    download_type    = models.CharField(
                                max_length=2,
                                null=True,
                                blank=True,
                                choices=DownloadType.choices,
                                default=DownloadType.File)
    force_download   = models.BooleanField(default=False)

    search_fields = [
        index.AutocompleteField('url'),
        index.FilterField('download_type'),
        index.FilterField("workspace"),
    ]

    api_fields = [ 
        APIField('url'),
    ]

    def save(self, *args, **kwargs):

        # If the url is new or 
        if self.force_download:
            
            bs = CoreSettings.load()

            if self.download_type   in [ DownloadType.File, ]:
                bs.download_file(self.url)
            
            elif self.download_type in [ DownloadType.WebpagePdf, ]:
                bs.download_pdf(self.url)
            
            else:
                raise NotImplementedError()


            # Switch off self.force_download flag
            self.force_download = False

            # Actually save record
            super().save(*args, **kwargs)
            
            # Delete old file 
            File.objects.filter(workspace_id=self.workspace_id, origin_url_id=self.id).delete()
            
            # Create and link new file
            fl = File()
            fl.workspace_id = self.workspace_id
            fl.origin_url_id = self.id
            fl.title         = bs.title
            fl.file          = bs.content_file
            fl.mimetype      = bs.mimetype
            fl.save()
            
        else:
            super().save(*args, **kwargs)


    def __str__(self):
        return self.url

    class Meta:
        verbose_name = "Origin"
