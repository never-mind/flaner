
import filetype
import tempfile
import string
import subprocess
import requests
from urllib.parse                    import urlparse
from pathlib                         import Path
from django.db                       import models
from django.core.files.base          import ContentFile
from django.utils.crypto             import get_random_string
from django.conf                     import settings
from wagtail.contrib.settings.models import BaseGenericSetting
import os

class UserAgents(models.TextChoices):
    Firefox_Windows = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/113.0', 'Firefox Windows'
    Chrome_Windows  = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'Chrome Windows'
    Edge_Windows    = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36 Edg/114.0.1823.41', 'Edge Windows'
    Safari_MacOSX   = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.5 Safari/605.1.1', 'Safari Mac OSX'


class CoreSettings(BaseGenericSetting):
    '''CoreSettings
    
    Various admin settings

    '''
    proxy        = models.CharField(max_length=255, null=True, blank=True)
    proxy_bypass = models.TextField(null=True, blank=True, default="localhost")
    user_agent   = models.TextField(choices=UserAgents.choices, default=UserAgents.Firefox_Windows)

    _url      = ""
    _title    = ""
    _driver   = None
    _content  = b""
    _mimetype = ""

    @property
    def title(self):
        if not self._title and self._url:
            self._title = Path(urlparse(self._url).path).name
        if not self._title:
            self._title = get_random_string(8, string.ascii_lowercase)
        return self._title

    @property
    def proxy_dict(self):
        return dict(
                http=self.proxy,
                https=self.proxy,
                no_proxy=self.proxy_bypass)

    @property
    def chromium_cli_base(self):
        """
        We use a base command to define custom chromium profile.

        That way we would expect to run headful chromium and browse persist our cookies and set the
        profile as we wish. Then running in headless with that profile we could print pdfs from that
        profile.

        Unforttunatelly that is not the case. Chromium behaves differently in headless and headful 
        context. Another approach xvfb-run without headless doesnt not work as well as it wont
        close the browser after printing.
        """
        args = [ "chromium",  
                # "--profile-directory=Content-bot"
                # f"--profile-directory={ os.path.join(settings.MEDIA_ROOT, 'chromium/profile') }"
                f"--user-data-dir={ os.path.join(settings.MEDIA_ROOT, 'chromium/content-user-data') }"
            ]
        if self.proxy:
            args.append(f"--proxy-server={self.proxy}")
        return args
    
    def download_pdf(self, url):
        temp_filename = next(tempfile._get_candidate_names()) + '.pdf'
        temp_filepath = '/'.join([tempfile._get_default_tempdir(), temp_filename ])
        args = [
                # 'xvfb-run'
            ] + self.chromium_cli_base + [
                    # "--headless",
                    "--headless=new",
                    "--disable-gpu",
                    "--no-sandbox",
                    "--disable-dev-shm-usage",
                    "--disable-setuid-sandbox",
                    "--run-all-compositor-stages-before-draw",

                    f"--print-to-pdf={temp_filepath}",
                    url
                ]
        print(' ---> ', ' '.join(args))

        subprocess.run(args)
        self._content  = open(temp_filepath, 'rb').read()
        self._url      = url
        self._title    = temp_filename
        self._mimetype = "application/pdf"

    def download_file(self, url):
        # TODO:
        # 1. Implement checks for html content and file size.
        # 2. Follow Content-Disposition for attached file name if any
        #
        # An nice tutorial is here 
        # https://www.codementor.io/@aviaryan/downloading-files-from-urls-in-python-77q3bs0un
        session = requests.Session()
        session.headers['user-agent'] = self.user_agent
        if self.proxy:
            session.proxies = self.proxy_dict
        resp = session.get(url)
        self._url     = url
        self._content = resp.content

    @property
    def content_file(self):
        return ContentFile( self._content, self.title)

    @property
    def mimetype(self):
        if not self._mimetype:
            self._mimetype = filetype.guess_mime(self._content)
        return self._mimetype