from django.db import models
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields  import ParentalKey
from taggit.models        import TagBase, ItemBase
from modelcluster.models  import ClusterableModel
from wagtail.models       import Orderable

from .base import Base

class PresentationType(models.TextChoices):
    map      = 'M', 'Map'
    graph    = 'G', 'Graph'
    timeline = 'T', 'Timeline'

class Presentation(Base, ClusterableModel, Orderable, TagBase):
    free_tagging = False
    presentation_type = models.CharField(
                                max_length=2,
                                choices=PresentationType.choices,
                                default=PresentationType.graph)

    class Meta:
        verbose_name = "Presentation"
        verbose_name_plural = "Presentations"

class PresentationFile(Base, Orderable, ItemBase):
    tag = ParentalKey(Presentation, related_name="files", on_delete=models.CASCADE)
    content_object = ParentalKey(to='core.File', on_delete=models.CASCADE, related_name='file_presentations')

class PresentationEntity(Base, Orderable, ItemBase):
    tag = ParentalKey(Presentation, related_name="entities", on_delete=models.CASCADE)
    content_object = ParentalKey(to='core.Entity', on_delete=models.CASCADE, related_name='entity_presentations')

class PresentationLocation(Base, Orderable, ItemBase):
    tag = ParentalKey(Presentation, related_name="locations", on_delete=models.CASCADE)
    content_object = ParentalKey(to='core.Location', on_delete=models.CASCADE, related_name='location_presentations')

class PresentationEvent(Base, Orderable, ItemBase):
    tag = ParentalKey(Presentation, related_name="events", on_delete=models.CASCADE)
    content_object = ParentalKey(to='core.Event', on_delete=models.CASCADE, related_name='event_presentations')
