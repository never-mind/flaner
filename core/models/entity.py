from django.db                    import models
from django.urls                  import reverse
from wagtail.models               import Orderable, RevisionMixin, LockableMixin
from wagtail.search               import index
from wagtail.admin.panels         import FieldPanel, InlinePanel, TabbedInterface, ObjectList, FieldRowPanel, MultipleChooserPanel, MultiFieldPanel
from modelcluster.models          import ClusterableModel
from modelcluster.contrib.taggit  import ClusterTaggableManager
from modelcluster.fields          import ParentalKey
from wagtail.api                  import APIField

from .base import *

class EntityClassification(IconMixin, Base):
    name   = models.CharField(max_length=255, unique=True, null=False, blank=False)

    panels = [
        FieldPanel('name'),
        FieldPanel('icon'),
    ]
    
    def __str__(self):
        return self.name


class EntityAttribute(index.Indexed, ClusterableModel, Orderable, Base):
    """EntityAttribute
 
    """
    entity    = ParentalKey('core.Entity',    related_name='ea_attributes', on_delete=models.CASCADE)
    attribute = ParentalKey('core.Attribute', related_name='ea_entities',   on_delete=models.CASCADE)

    panels = [
        FieldPanel('entity'),
        FieldPanel('attribute'),
    ]

    @cached_property
    def name(self):
        return self.attribute.name

    @cached_property
    def value(self):
        return self.attribute.value

    def __str__(self):
        return f"{self.attribute.name} : {self.attribute.value}"


class Entity(AvatarMixin, index.Indexed, RevisionMixin, ClusterableModel, Base):
    
    classification = models.ForeignKey('core.EntityClassification', null=False, blank=False, on_delete=models.CASCADE, related_name='entity')
    created_at     = models.DateTimeField(verbose_name="created at", auto_now_add=True)
    presentations  = ClusterTaggableManager(through='core.PresentationEntity', verbose_name="presentations", blank=True)
    description    = models.TextField(null=True, blank=True, default='')

    @cached_property
    def reference(self):
        return self.ea_attributes.first().attribute.value

    undefined_avatar_icon = 'image'

    search_fields = [
        index.FilterField('classification'),
        index.RelatedFields('ea_attributes', [
            index.RelatedFields('attribute', [
                index.AutocompleteField('value'),
            ]),
        ]),
    ]

    api_fields = [ 
        APIField('reference'),
        APIField('classification'),
        APIField('presentations'),
        APIField('ea_attributes'),
    ]

    class Meta:
        verbose_name = "Entity"
        verbose_name_plural = "Entities"
    
    def __str__(self):
        return f"{self.classification } : { self.reference }"

    @cached_property
    def admin_thumb(self):
        """
        Generate thumbnail for entity.
        """
        url = self.admin_thumb_url
        if not url:
            return self.classification.admin_thumb_icon
        else:
            return mark_safe("<img{}>".format(flatatt({
                "src": url,
                "width": 40,
                "class": 'admin-thumb',
                "decoding": "async",
                "loading": "lazy",
                "style": "border-radius: 5px;",
            })))

    # @cached_property
    # def admin_thumb_url(self):
    #     """
    #     Return Entity's avatar url if defined otherwise use Category's avatar url or empty if both fields are undefined.
    #     """
    #     if self.avatar:
    #         return get_rendition_or_not_found(self.avatar, 'fill-100x100').url
    #     elif self.classification.avatar:
    #         return get_rendition_or_not_found(self.category.avatar, 'fill-100x100').url
    #     else:
    #         return ''

    # main = models.ForeignKey('entity.EntityAttribute', null=False, blank=False, on_delete=models.CASCADE, related_name='main_entity')
    # description    = models.TextField(null=True, blank=True)
    # weight  = models.IntegerField(default=0)
    # name = models.CharField(max_length=255)
    # def compute_weight(self):
    #     """
    #     Compute weight based on
    #     1. Weight of related Attributes 
    #     2. Weight of related Documents
    #     3. 
    #     """
    #     return 0