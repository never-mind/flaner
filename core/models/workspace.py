from django                     import forms
from django.db                  import models
from django.db.models           import Min
from django.utils.html          import format_html
from django.utils.safestring    import mark_safe
from django.utils.functional    import cached_property
from wagtail.api                import APIField
from wagtail.search             import index
from treebeard.mp_tree          import MP_Node
from django.core.exceptions     import PermissionDenied

from .base import Base

class Workspace(Base, MP_Node):
    name = models.CharField(max_length=255, verbose_name="name")
    description = models.TextField(null=True, blank=True, default='')

    node_order_by = ["name"]

    def get_indented_name(self, indentation_start_depth=2, html=False):
        """
        Renders this Collection's name as a formatted string that displays its hierarchical depth via indentation.
        If indentation_start_depth is supplied, the Collection's depth is rendered relative to that depth.
        indentation_start_depth defaults to 2, the depth of the first non-Root Collection.
        Pass html=True to get a HTML representation, instead of the default plain-text.

        Example text output: "    ↳ Pies"
        Example HTML output: "&nbsp;&nbsp;&nbsp;&nbsp;&#x21b3 Pies"
        """
        display_depth = self.depth - indentation_start_depth
        # A Collection with a display depth of 0 or less (Root's can be -1), should have no indent.
        if display_depth <= 0:
            return self.name

        # Indent each level of depth by 4 spaces (the width of the ↳ character in our admin font), then add ↳
        # before adding the name.
        if html:
            # NOTE: &#x21b3 is the hex HTML entity for ↳.
            return format_html(
                "{indent}{icon} {name}",
                indent=mark_safe("&nbsp;" * 4 * display_depth),
                icon=mark_safe("&#x21b3"),
                name=self.name,
            )
        # Output unicode plain-text version
        return "{}↳ {}".format(" " * 4 * display_depth, self.name)

    @cached_property
    def indented_name(self):
        return self.get_indented_name(html=True)
    
    indented_name.short_description = "name"

    @cached_property
    def parent(self):
        return self.get_parent()

    parent.short_description = "parent"

    def __str__(self):
        return self.get_indented_name(html=True)

    class Meta:
        verbose_name = "workspace"
        verbose_name_plural = "workspaces"
    
    def delete(self):
        """Prevent users from deleting the root node."""
        if self.is_root():
            raise PermissionDenied('Cannot delete root Topic.')
        else:
            super().delete()
    
    api_fields = [
        APIField('id'),
        APIField('name'),
    ]

def get_root_workspace_id():
    return Workspace.get_first_root_node().id


class WorkspaceChoiceField(forms.ModelChoiceField):
    
    widget = forms.Select

    def __init__(self, *args, disabled_queryset=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._indentation_start_depth = 2

    def _set_queryset(self, queryset):
        min_depth = self.queryset.aggregate(Min("depth"))["depth__min"]
        if min_depth is None:
            self._indentation_start_depth = 2
        else:
            self._indentation_start_depth = min_depth + 1

    def label_from_instance(self, obj):
        return obj.get_indented_name(self._indentation_start_depth, html=True)


class WorkspaceMember(models.Model):

    workspace = models.ForeignKey(
        Workspace,
        default=get_root_workspace_id,
        verbose_name="workspace",
        related_name="+",
        on_delete=models.CASCADE,
    )

    search_fields = [
        index.FilterField("workspace"),
    ]

    class Meta:
        abstract = True
