from django.db                  import models 
from django.utils.functional    import cached_property
from wagtail.admin.panels       import *
from wagtail.models             import ClusterableModel
from wagtail.search             import index
from wagtailgeowidget.helpers   import geosgeometry_str_to_struct
from django.conf import settings
from .base import Base


class Location(ClusterableModel, index.Indexed, Base):
    address  = models.CharField(max_length=250, blank=True, null=True, help_text="Search powered by Nominatim",)
    location = models.CharField(max_length=250, blank=True, null=True)
    zoom     = models.SmallIntegerField(blank=True, null=True, default=settings.DEFAULT_GEO_WIDGET_ZOOM)
    description = models.TextField(null=True, blank=True, default='')

    @cached_property
    def point(self):
        return geosgeometry_str_to_struct(self.location)

    @property
    def lat(self):
        return self.point['y']

    @property
    def lng(self):
        return self.point['x']

    def __str__(self):
        return self.address

    search_fields = [
        index.SearchField("address", boost=10),
        index.AutocompleteField("address"),
        index.AutocompleteField("description"),
        index.AutocompleteField("location"),
    ]