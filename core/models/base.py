import re
import uuid

from django.db                    import models
from django.utils.safestring      import mark_safe
from django.forms.utils           import flatatt
from django.utils.functional      import cached_property
from django.core.exceptions       import ValidationError
from wagtail.search               import index
from wagtail.models               import Orderable
from wagtail.images.shortcuts     import get_rendition_or_not_found
from modelcluster.models          import ClusterableModel
from modelcluster.fields          import ParentalKey
from django.urls                  import reverse
from django.contrib.admin.utils   import quote


class Base(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    @cached_property
    def admin_url(self):
        return reverse(f'wagtailsnippets_{self._meta.app_label}_{self._meta.model_name}:edit', args=(quote(self.pk),))

    class Meta:
        abstract=True


class IconMixin(models.Model):
    icon = models.CharField(
                        max_length=255, 
                        null=True,
                        blank=True,
                        help_text="List of icons can be viewed under /styleguide/#icons-section ",
                        choices=[ 
                            ('feather', 'feather'),
                            ('flag', 'flag'),
                            ('file-pdf', 'file-pdf'),
                            ('file-video', 'file-video'),
                            ('fingerprint', 'fingerprint'),
                            ('receipt', 'receipt'),
                            
                            ('user',  'user'),
                            ('person',  'person'),
                            ('people-line', 'people-line'),
                            ('group', 'group'),
                            
                            ('mail', 'mail'), 
                            ('phone', 'phone'),
                            ('passport', 'passport'),
                            ('credit-card', 'credit-card'),
                            ('ethernet', 'ethernet'),
                            
                            ('plane', 'plane'),
                            ('ship', 'ship'),
                            ('motorcycle', 'motorcycle'),
                            ('train', 'train'),
                            ('car', 'car'),
                            ('building', 'building'),
                            ('google', 'google'),
                            ('apple', 'apple'),
                            ('windows', 'windows'),
                            ('paypal', 'paypal'),
                            ('stripe', 'stripe'),
                            ('airbnb', 'airbnb'),
                            ('linux', 'linux'),
                            ('gitlab', 'gitlab'),
                            ('amazon', 'amazon'),
                            ('twitter', 'twitter'),
                            ('youtube', 'youtube'),
                            ('github', 'github'),
                            ('linkedin', 'linkedin'),
                            ('facebook', 'facebook'),
                            ('instagram', 'instagram'),
                            ('discord', 'discord'),
                            ('whatsapp', 'whatsapp'),
                            ('vimeo', 'vimeo'),
                            ('telegram', 'telegram'),
                            ('pinterest', 'pinterest'),
                            ('skype', 'skype'),
                            ('spotify', 'spotify'),
                            ('twitch', 'twitch'),
                        ])

    @cached_property
    def admin_thumb_icon(self):
        return self.get_admin_thumb_icon()

    def get_admin_thumb_icon(self, 
                                    width=40,
                                    # color='var(--w-color-text-label)',
                                    color='var(--w-color-primary-200)',
                                    # color='var(--w-color-secondary)',                                    
                                    stroke_color='var(--w-color-primary)',
                                    stroke_width='0px',
                                    background_color="var(--w-color-surface-page)"
                                ):
        return mark_safe("""<svg 
                                class="icon icon-{icon_name}"
                                aria-hidden="true"
                                width={width}
                                height={width}                                
                                style="
                                    background-color:{background_color};
                                ">
                                <use 
                                    href="#icon-{icon_name}" 
                                    fill="{color}"
                                    stroke="{stroke_color}"
                                    stroke-width="{stroke_width}" ></use>
                            </svg>""".format(width=width, color=color, stroke_color=stroke_color, stroke_width=stroke_width, background_color=background_color, icon_name=self.icon))

    admin_thumb_icon.short_description = "icon"

    class Meta:
        abstract=True


class AvatarMixin(IconMixin, models.Model):
    """
    Helpful abstract class to add avatar field.
    """
    
    avatar  = models.ForeignKey('wagtailimages.Image', null=True, blank=True, on_delete=models.SET_NULL, related_name='+')

    @cached_property
    def admin_thumb_url(self):
        """
        Return thumbnail url if avatar image is set or none.
        """
        if self.avatar:
            return get_rendition_or_not_found(self.avatar, 'fill-100x100').url
        else:
            return ''

    @cached_property
    def admin_thumb(self):
        """
        Generate thumbnail for entity.
        """
        url = self.admin_thumb_url
        if not url:
            return self.admin_thumb_icon
        else:
            return mark_safe("<img{}>".format(flatatt({
                "src": url,
                "width": 40,
                "class": 'admin-thumb',
                "decoding": "async",
                "loading": "lazy",
                "style": "border-radius: 5px;",
            })))
    
    admin_thumb.short_description = "avatar"

    class Meta:
        abstract = True


class AttributeName(IconMixin, index.Indexed, ClusterableModel, Base):
    """AttributeName 
    
    AttributeName model defines what type of attributes a user can create.

    It is supposed to be set by admins or previledged users and rarely change.

    `name` is the actual attribute name that will be used. Valid examples are considered 
        - Legal Name
        - Specific Country's Vat registration number
        - Specific Country's Phone Number
        - Email Address
        - ...
    `regex` is an optional field that is used by Attribute Model to validate entries.
    `description` is an optional field giving more details and link to official resources
    `kind` TBD

    """
    name        = models.CharField(max_length=255, unique=True, null=False, blank=False)
    pattern     = models.TextField(blank=True, null=True, help_text="Regex pattern to validate")
    description = models.TextField(null=True, blank=True)

    search_fields = [
        index.AutocompleteField('name'),
    ]
    
    def __str__(self):
        return self.name


class Attribute(index.Indexed, ClusterableModel, Orderable, Base):
    """Attribute

    Attribute model holds a key-value store. 

    `key` points to AttributeName
    `value` is an arbitary value that will be checked against related AttributeName's regex.

    """
    
    key    = ParentalKey('core.AttributeName', null=False, blank=False, on_delete=models.CASCADE, related_name='attributes')
    value  = models.TextField(null=False, blank=False)

    search_fields = [
        index.AutocompleteField('value', boost=10),
        index.RelatedFields('key', [
            index.FilterField('name'),
        ]),
        index.RelatedFields('fa_files', [
            index.AutocompleteField('title'),
        ]),
    ]
    
    def clean(self):
        if self.key.pattern:
            if not re.match(self.key.pattern, self.value):
                raise ValidationError({"value": f" \"{self.value}\" doen't match expected pattern ( {self.key.pattern} )."})
    
    @cached_property
    def name(self):
        return self.key.name


    def __str__(self):
        return f"{self.name}: {self.value}"
