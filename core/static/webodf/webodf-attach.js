function init() {
    document.querySelectorAll('[preview-odf-url]').forEach(function(odfelement) {
        odfcanvas = new odf.OdfCanvas(odfelement);
        // odfcanvas.addListener("statereadychange", function (err) {
        //     console.log(err);
        // });
        odfcanvas.load(
            odfelement.getAttribute('preview-odf-url')
        );
    });
}
window.setTimeout(init, 0);