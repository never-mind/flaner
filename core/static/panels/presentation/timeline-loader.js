window.onload = (event) => {

    var timeline_data = JSON.parse(document.getElementById("presentation-data").innerText);

    window.timeline = new TL.Timeline('presentation-timeline', timeline_data,
        {
            start_at_end: true,
            // default_bg_color: {r:0, g:0, b:0},
            // timenav_height: 500
            timenav_height_percentage: 50,
            dragging:true,
            trackResize:true, 
        }
    );

};