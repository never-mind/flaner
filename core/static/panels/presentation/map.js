window.onload = (event) => {
    
    
    var raw_data = JSON.parse(document.getElementById("presentation-data").innerText);
    // console.log(raw_data);
    
    // Set view central point to Athens
    const map = L.map('presentation-map').setView([37.99083,23.6971399], 10);

    // Set tiles
    const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);
    
    // Add markers to all related locations
    raw_data.locations.forEach(function(location) {
        
        // Generate popup content for given location
        var location_summary = "";
        var files_summary = "";
        if ( location.files.length > 0 ) {
            files_summary = files_summary + 'Files : <br><ul>'
            location.files.forEach(function(fl) {
                files_summary = files_summary + `<li> <a href=${fl.url}>${fl.title}</a> (${fl.workspace})</li>`;
            })
            files_summary = files_summary + '</ul>'
        }
        location_summary = location_summary +`
            <a href="${location.url}" target="_blank" rel="noopener noreferrer">${location.address}</a><br>
            <br>
            Latitude: ${ location.lat }<br>
            Longitude: ${ location.lng }<br>
            ${files_summary}
            Description: <i>${ location.description }</i><br>
        `;

        // Create marker, add it to the map and bind generated popup from above
        L.marker([location.lat, location.lng])
            .addTo(map)
            .bindPopup(location_summary);
    })

    // Add default popup window
    var popup = L.popup();

    function onMapClick(e) {
        popup
            .setLatLng(e.latlng)
            .setContent("You clicked the map at " + e.latlng.toString())
            .openOn(map);
    }

    map.on('click', onMapClick);

};