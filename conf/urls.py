from django.conf                     import settings
from django.urls                     import include, path, re_path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from wagtail.admin                   import urls as wagtailadmin_urls
from wagtail                         import urls as wagtail_urls
from wagtail.documents               import urls as wagtaildocs_urls

from enhancements.admin import urlpatterns as wagtail_custom_admin_urls
from enhancements.views import (
                protected_media_serve,
                serve_docs,
                serve_docs_frame
            )

from api.router         import api_router, rest_router
from api.views          import graphql_view
from core.views         import file_serve, file_preview


urlpatterns = [
    
        # Enable Rest and Graphql Api
        path('api/', include(rest_router.urls)),
        path('api/v2/', api_router.urls),
        path("api/graphql", graphql_view),

        # Use custom pretected view to serve Wagtail images 
        re_path(r"^media/(?P<path>.*)$", protected_media_serve,
                                            {
                                                'allowed_prefixes': [
                                                    '/media/images/',
                                                    '/media/avatar_images/',
                                                ],
                                                'document_root': settings.MEDIA_ROOT,
                                                'show_indexes': False 
                                            },
                                            name="protected_media"),
        
        # Use custom file view to serve files with group permissions.
        path("file/<uuid:file_id>", file_serve, name="file_serve"),
        path("file-thumb/<uuid:file_id>", file_serve, { 'serve_thumbs': True }, name="file_thumb_serve"),
        # path("file/<uuid:file_id>/preview", file_preview, name="file_preview"),

        # Use pretected Documentation view to show mkdocs build output
        re_path(r"^docs/(?P<path>.*)$", serve_docs, {'document_root': settings.MKDOCS_DIR, 'show_indexes': True}, name="mkdos"),

        # # Enable page serve view limited to authenticated users.
        # re_path(
        #     r"^([\w\-/]*)$",           # WAGTAIL_APPEND_SLASH = False
        #     # r"^((?:[\w\-]+/)*)$",    # WAGTAIL_APPEND_SLASH = True
        #     protected_page_serve,
            
        #     # Use default name view otherwise it breaks the [view-live] button on 
        #     # admin Pages Explorer. 
        #     name="wagtail_serve"
        # ),

        # Serve Custom Wagtail Admin at Root
        path("", include(wagtail_custom_admin_urls)),

    ] \
    + staticfiles_urlpatterns()
