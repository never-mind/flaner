from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "django-insecure-#d11-u+-swtx3eu&5x94$m!x^3pa6_oyr5#a=_h_+=h#q&e-95"

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ["*"]

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        # 'ENGINE': 'django.contrib.gis.db.backends.spatialite',
        "NAME": os.environ.get( 'DB_PATH', os.path.join( os.path.dirname(BASE_DIR), "django.sqlite3") ),
    }
}
