from .base import *
import os 

DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY    = os.environ.get('DJANGO_SECRET_KEY')
ALLOWED_HOSTS = os.environ.get('DJANGO_ALLOWED_HOSTS', '*').split(',')
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        'NAME': os.environ.get('DJANGO_DB_NAME', 'flaner'), 
        # By default empty, will use unix socket
        'USER':     os.environ.get('DJANGO_DB_USER', 'flaner'),
        'PASSWORD': os.environ.get('DJANGO_DB_PASSWORD', ''),
        'HOST':     os.environ.get('DJANGO_DB_HOST',     ''),
        'PORT':     os.environ.get('DJANGO_DB_PORT',     ''),
    }
}

# PostGIS example
# 
# INSTALLED_APPS += ["django.contrib.gis"]
# DATABASES = {
#     "default": {
#         "ENGINE": "django.contrib.gis.db.backends.postgis",
#         ...
#     }
# }

DATA_DIR    = os.environ['DJANGO_DATA_DIR']
STATIC_ROOT = os.path.join(DATA_DIR, "static")
MEDIA_ROOT  = os.path.join(DATA_DIR, "media")
STATICFILES_DIRS = []

CSRF_TRUSTED_ORIGINS = os.environ.get('DJANGO_CSRF_TRUSTED_ORIGINS', '').split(',')

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.memcached.PyMemcacheCache",
        "LOCATION": os.environ.get('DJANGO_MEMCACHE_LOCATION', '127.0.0.1:11211').split(','),
    }
}

USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')