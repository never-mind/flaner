from django.utils.safestring import mark_safe
from django.urls             import reverse
from wagtail                 import hooks
from wagtail.snippets        import widgets as wagtailsnippets_widgets
from wagtail.admin.menu      import MenuItem

# Add more icons
@hooks.register("register_icons")
def register_icons(icons):
    return icons + [
        
        'enhancements/icons/feather.svg',
        'enhancements/icons/flag.svg',
        'enhancements/icons/file-pdf.svg',
        'enhancements/icons/file-video.svg',
        'enhancements/icons/person.svg',
        'enhancements/icons/people-line.svg',

        'enhancements/icons/fingerprint.svg',
        'enhancements/icons/receipt.svg',
        'enhancements/icons/phone.svg',
        'enhancements/icons/passport.svg',
        'enhancements/icons/credit-card.svg',
        'enhancements/icons/ethernet.svg',

        'enhancements/icons/plane.svg',
        'enhancements/icons/ship.svg',
        'enhancements/icons/motorcycle.svg',
        'enhancements/icons/train.svg',
        'enhancements/icons/car.svg',
        'enhancements/icons/building.svg',


        'enhancements/icons/google.svg',
        'enhancements/icons/apple.svg',
        'enhancements/icons/windows.svg',
        'enhancements/icons/paypal.svg',
        'enhancements/icons/stripe.svg',
        'enhancements/icons/airbnb.svg',
        'enhancements/icons/linux.svg',
        'enhancements/icons/gitlab.svg',
        'enhancements/icons/amazon.svg',

        # Social Media
        'enhancements/icons/twitter.svg',
        'enhancements/icons/tiktok.svg',
        'enhancements/icons/youtube.svg',
        'enhancements/icons/github.svg',
        'enhancements/icons/linkedin.svg',
        'enhancements/icons/facebook.svg',
        'enhancements/icons/instagram.svg',
        'enhancements/icons/discord.svg',
        'enhancements/icons/whatsapp.svg',
        'enhancements/icons/vimeo.svg',
        'enhancements/icons/telegram.svg',
        'enhancements/icons/pinterest.svg',
        'enhancements/icons/skype.svg',
        'enhancements/icons/spotify.svg',
        'enhancements/icons/twitch.svg',
    ]

# UI Customizations of wagtail admin
# ==================================

@hooks.register('construct_main_menu')
def hide_default_menu_item(request, menu_items):
    if request.user.is_superuser:
        menu_items[:] = [
            item for item in menu_items if item.name not in [
                'images',
                'documents',
                'explorer', 
                'reports',

            ]
        ]
    else:
        menu_items[:] = [
            item for item in menu_items if item.name not in [
                'snippets',
                'images',
                'documents',
                'settings',
                'explorer',
                'reports',
                'help',
                'explorer',
            ]
        ]

from django.urls import path
from .views import serve_docs_frame 
@hooks.register('register_admin_urls')
def register_docs_url():
    return [
        path('docs-frame/', serve_docs_frame, name='docs-frame'),
    ]

@hooks.register('register_admin_menu_item')
def register_documentation_menu_item():
    '''
    Register menu item to point users to internal Documentation link  
    '''
    return MenuItem('Documentation', reverse('docs-frame'), icon_name='info-circle', order=10000)

@hooks.register('insert_global_admin_css')
def customize_admin_colours():
    '''
    Customize wagtail's admin UI colors, to grayscale. 
    '''
    return mark_safe("""
<style>
:root {

--w-color-primary: #262626; 
--w-color-primary-200: #5C5C5C;


/*

--w-color-secondary: #929292;
--w-color-secondary-600: #262626;
--w-color-secondary-400: #5C5C5C;
--w-color-secondary-100: #C8C8C8;
--w-color-secondary-75: #E0E0E0;
--w-color-secondary-50: #F6F6F6;

*/


/* Fonts 
--w-font-sans: Papyrus;
--w-font-mono: Courier;
*/

/* By switching to grayscale on dark theme,
    the check buttons break, so we change them to use a
    strong color.
*/
--w-color-border-button-outline-default: var(--w-color-critical-200);

}

html {
    font-size: 95% !important;
}
</style>
""")
