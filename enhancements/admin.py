from django.urls              import include, path, re_path
from wagtail.admin.views.home import SiteSummaryPanel, UpgradeNotificationPanel, RecentEditsPanel, HomeView
from wagtail.admin.urls       import urlpatterns as wagtailadmin_urlpatterns
from wagtail                  import hooks
from wagtail.admin.auth              import require_admin_access

class AdminHomeView(HomeView):

    def get_panels(self):
        request = self.request
        panels = [
            # SiteSummaryPanel(request),

            # Disabled until a release warrants the banner.
            # WhatsNewInWagtailVersionPanel(),
            # UpgradeNotificationPanel(),
            # WorkflowObjectsToModeratePanel(),
            # PagesForModerationPanel(),
            # UserObjectsInWorkflowModerationPanel(),
            # RecentEditsPanel(),
            # LockedPagesPanel(),
        ]

        for fn in hooks.get_hooks("construct_homepage_panels"):
            fn(request, panels)
        return panels

urlpatterns = [
        # path("", home.HomeView.as_view(), name="wagtailadmin_home"),
        path("",     require_admin_access(  AdminHomeView.as_view() ), name="wagtailadmin_home"),
    ] + wagtailadmin_urlpatterns[1:]