import os
from django.conf                     import settings
from django.core.exceptions          import PermissionDenied
from django.views.static             import serve
from django.shortcuts                import render
from django.http                     import Http404, HttpResponse
from wagtail                         import hooks
from wagtail.models                  import Page, PageViewRestriction, Site
from wagtail.admin.auth              import require_admin_access
from django.contrib.auth.decorators  import login_required

@require_admin_access
def protected_page_serve(request, path):
    # we need a valid Site object corresponding to this request in order to proceed
    site = Site.find_for_request(request)
    if not site:
        raise Http404

    path_components = [component for component in path.split("/") if component]
    page, args, kwargs = site.root_page.localized.specific.route(
        request, path_components
    )

    for fn in hooks.get_hooks("before_serve_page"):
        result = fn(page, request, args, kwargs)
        if isinstance(result, HttpResponse):
            return result

    return page.serve(request, *args, **kwargs)



def protected_media_serve(request, path, allowed_prefixes=[], disallowed_prefixes=[], **kwargs):
    """
    This is a simple view that serves static file from MEDIA_ROOT to authenticated users 
    that have access to admin UI only. 

    Additionally utilizes `allowed_prefixes` and `disallowed_prefixes` lists to limit the
    scope of subdirectories that are allowed to be served. 

    """
    user = request.user

    if user.is_anonymous:
        raise PermissionDenied

    if user.has_perms(["wagtailadmin.access_admin"]):
        
        if allowed_prefixes:
            if len([ True for p in allowed_prefixes if request.path.startswith(p) ]) == 0:
                raise PermissionDenied

        if disallowed_prefixes:
            if len([ True for p in disallowed_prefixes if request.path.startswith(p) ]) > 0:
                raise PermissionDenied

        return serve(request, path, **kwargs)
    
    raise PermissionDenied


@login_required
def serve_docs(request, path, **kwargs):
    '''
    Serves static generated files ( used by mkdocs ) behind authentication.  
    '''
    if os.path.isdir(
            os.path.join(settings.MKDOCS_DIR, path)
        ):
        path = os.path.join(path, 'index.html')
    return serve(request, path, **kwargs)


@login_required
def serve_docs_frame(request, **kwargs):
    '''
    Serves documentation under admin UI.  
    '''
    return render(request, 'enhancements/docs/iframe.html')
